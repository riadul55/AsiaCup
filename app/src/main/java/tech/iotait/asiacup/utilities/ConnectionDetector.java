package tech.iotait.asiacup.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.IOException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import tech.iotait.asiacup.interfaces.ConnectionListener;

public class ConnectionDetector {
    private Context _context;
    private ConnectionListener _ConnectionListener;

    private static final String ONLINE_LOCATION = "https://google.com";

    public ConnectionDetector(Context context){
        this._context = context;
    }

    public ConnectionDetector(Context context, ConnectionListener connectionListener){
        this._context = context;
        this._ConnectionListener = connectionListener;
    }

    public void isConnectingToInternet(){
        String msg = "No Internet Connection!";
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
//            NetworkInfo[] info = connectivity.getAllNetworkInfo();
//            if (info != null) {
//                for (int i = 0; i < info.length; i++){
//                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
//                        _ConnectionListener.onConnectionSuccess();
//                    }
//                }
//            }

            NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()){
                _ConnectionListener.onConnectionSuccess();
            } else {
                _ConnectionListener.onConnectionFail(msg);
            }
        } else {
            if (_context == null) {
                msg = "Context is null";
            }
            _ConnectionListener.onConnectionFail(msg);
        }
    }

    public boolean isConnected(){
        String msg = "No Internet Connection!";
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public static boolean makeNetworkCall() {
        try {
            URL url = new URL(ONLINE_LOCATION);
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.getInputStream();
            Log.d("ConnectionDetector", "Network call completed.");
            return true;
        } catch (IOException e) {
            Log.e("ConnectionDetector", "IOException " + e.getMessage());
            return false;
        }
    }
}
