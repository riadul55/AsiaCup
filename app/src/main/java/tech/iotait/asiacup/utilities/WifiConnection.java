package tech.iotait.asiacup.utilities;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.CountDownTimer;
import android.widget.Toast;

public class WifiConnection {
    private Context context;

    public WifiConnection(final Context context) {
        this.context = context;
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(true);

        new CountDownTimer(10000, 1000) {
            public void onTick(long millisUntilFinished) {
                Toast.makeText(context, "Connecting Internet...." + millisUntilFinished / 1000, Toast.LENGTH_SHORT).show();
            }
            public void onFinish() {
                if (!new ConnectionDetector(context).isConnected()){
                    Toast.makeText(context, "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        }.start();
    }
}
