package tech.iotait.asiacup.utilities;

import android.content.Context;
import android.net.wifi.WifiConfiguration;

public class ConnectWifi {
    private Context context;

    private void connectWifi(Context context) {
        this.context = context;


//        try {
//            WifiConfiguration conf = new WifiConfiguration();
//            conf.SSID = "\"" + networkSSID + "\"";   // Please note the quotes. String should contain ssid in quotes
//            conf.status = WifiConfiguration.Status.ENABLED;
//            conf.priority = 40;
//            // Check if security type is WEP
//            if (networkType.toUpperCase().contains("WEP")) {
//                Log.v("rht", "Configuring WEP");
//                showToast("Configuring WEP");
//                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
//                conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
//                conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
//                conf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
//                conf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
//                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
//                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
//                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
//                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
//
//                if (networkPass.matches("^[0-9a-fA-F]+$")) {
//                    conf.wepKeys[0] = networkPass;
//                } else {
//                    conf.wepKeys[0] = "\"".concat(networkPass).concat("\"");
//                }
//
//                conf.wepTxKeyIndex = 0;
//// Check if security type is WPA
//            } else if (networkType.toUpperCase().contains("WPA")) {
//                Log.v(TAG, "Configuring WPA");
//                showToast("Configuring WPA");
//
//                conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
//                conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
//                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
//                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
//                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
//                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
//                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
//                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
//                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
//
//                conf.preSharedKey = "\"" + networkPass + "\"";
//                // Check if network is open network
//            } else {
//                Log.v(TAG, "Configuring OPEN network");
//                showToast("Configuring OPEN network");
//
//                conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
//                conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
//                conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
//                conf.allowedAuthAlgorithms.clear();
//                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
//                conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
//                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
//                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
//                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
//                conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
//            }
//            //Connect to the network
//            WifiManager wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//            wifiManager.setWifiEnabled(true);
//            int networkId = wifiManager.addNetwork(conf);
//
//            Log.v(TAG, "Add result " + networkId);
//
//            List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
//            for (WifiConfiguration i : list) {
//                if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
//                    Log.v(TAG, "WifiConfiguration SSID " + i.SSID);
//                    showToast("WifiConfiguration SSID "  + i.SSID);
//
//                    boolean isDisconnected = wifiManager.disconnect();
//                    Log.v(TAG, "isDisconnected : " + isDisconnected);
//
//                    boolean isEnabled = wifiManager.enableNetwork(i.networkId, true);
//                    Log.v(TAG, "isEnabled : " + isEnabled);
//
//                    boolean isReconnected = wifiManager.reconnect();
//                    Log.v(TAG, "isReconnected : " + isReconnected);
//
//                    if (isReconnected){
//                        showToast("Wifi Connecting...");
//                    }
//
//                    break;
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        WifiConfiguration conf = new WifiConfiguration();
//        conf.SSID = "\"" + networkSSID + "\"";
//        conf.preSharedKey = "\""+ networkPass +"\"";
//        conf.wepKeys[0] = "\""+ networkPass +"\"";
//        conf.wepTxKeyIndex = 0;
//        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
//        conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
//        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
//        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
//        wifiManager.addNetwork(conf);
//        wifiManager.setWifiEnabled(true);
//        if (wifiManager.isWifiEnabled()){
//            if (wifiManager.isScanAlwaysAvailable()){
//                wifiManager.startScan();
//                scanResults = wifiManager.getScanResults();
//
//                Log.d("Scan Results ", scanResults.toString());
//            }
//        }
//
//        if (wifiManager.isWifiEnabled()){
//            List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
//            for( WifiConfiguration i : list) {
//                if(i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
//                    wifiManager.disconnect();
//                    wifiManager.enableNetwork(i.networkId, true);
//                    wifiManager.reconnect();
//                    break;
//                }
//            }
//        }
    }
}
