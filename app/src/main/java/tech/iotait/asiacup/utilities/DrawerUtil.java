package tech.iotait.asiacup.utilities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.model.ExpandableBadgeDrawerItem;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import tech.iotait.asiacup.R;
import tech.iotait.asiacup.activities.AboutActivity;
import tech.iotait.asiacup.activities.HomeActivity;
import tech.iotait.asiacup.activities.LiveMatchActivity;
import tech.iotait.asiacup.activities.LiveTvActivity;
import tech.iotait.asiacup.activities.PointTableGroupActivity;
import tech.iotait.asiacup.activities.Schedule;
import tech.iotait.asiacup.activities.TeamListActivity;


public class DrawerUtil {
    private Activity activity;
    private Toolbar toolbar;
    private AccountHeader header;
    private Drawer drawer;

    public Drawer getDrawer(final Activity activity, Toolbar toolbar){
        this.activity = activity;
        this.toolbar = toolbar;
        this.activity = activity;

        IProfile profile = new ProfileDrawerItem()
//                .withName("Rakib")
//                .withEmail("rakibbdm@gmail.com")
                .withIcon("https://avatars3.githubusercontent.com/u/1476232?v=3&s=460");

        header = new AccountHeaderBuilder().withActivity(activity)
                .withTranslucentStatusBar(true)
                .withHeaderBackground(R.drawable.header)
//                .addProfiles(profile)
                .withSelectionListEnabledForSingleProfile(false)
//                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
//                    @Override
//                    public boolean onProfileChanged(View view, IProfile profile, boolean current) {
                        //sample usage of the onProfileChanged listener
                        //if the clicked item has the identifier 1 add a new profile ;)
//                        if (profile instanceof IDrawerItem && profile.getIdentifier() == PROFILE_SETTING) {
//                            int count = 100 + headerResult.getProfiles().size() + 1;
//                            IProfile newProfile = new ProfileDrawerItem().withNameShown(true).withName("Batman" + count).withEmail("batman" + count + "@gmail.com").withIcon(R.drawable.ic_launcher_background).withIdentifier(count);
//                            profile();
//                            if (headerResult.getProfiles() != null) {
//                                //we know that there are 2 setting elements. set the new profile above them ;)
//                                headerResult.addProfile(newProfile, headerResult.getProfiles().size() - 2);
//                            } else {
//                                headerResult.addProfiles(newProfile);
//                            }
//                        }
                        //false if you have not consumed the event and it should close the drawer
//                        Toast.makeText(view.getContext(), "profile", Toast.LENGTH_SHORT).show();
//                        return false;
//                    }
//                })
                .build();

        drawer();

        return drawer;
    }

    private void drawer(){
        drawer = new DrawerBuilder()
                .withActivity(activity)
                .withToolbar(toolbar)
//                .withAccountHeader(header)
                .withHasStableIds(true)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withCloseOnClick(true)
                .withSelectedItem(-1)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("").withSelectable(false),
                        new PrimaryDrawerItem().withName("Home").withIcon(R.drawable.ic_home).withIdentifier(1).withSelectable(true),
                        new PrimaryDrawerItem().withName("Team").withIcon(R.drawable.team).withIdentifier(2).withSelectable(true),
                        new PrimaryDrawerItem().withName("Upcoming Match").withIcon(R.drawable.upcoming_match).withIdentifier(3).withSelectable(true),
                        new PrimaryDrawerItem().withName("Point Table").withIcon(R.drawable.news).withIdentifier(4).withSelectable(true),
                        new PrimaryDrawerItem().withName("Live Score").withIcon(R.drawable.livescore).withIdentifier(5).withSelectable(true),
                        new PrimaryDrawerItem().withName("Highlights").withIcon(R.drawable.highlight).withIdentifier(6).withSelectable(true),
                        new PrimaryDrawerItem().withName("Live TV").withIcon(R.drawable.livetv).withIdentifier(7).withSelectable(false),
                        new PrimaryDrawerItem().withName("Share").withIcon(R.drawable.ic_share).withIdentifier(8).withSelectable(false),
                        new PrimaryDrawerItem().withName("About").withIcon(R.drawable.about).withIdentifier(9).withSelectable(false)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem != null) {
                            Intent intent = null;
                            switch (position){
                                case 1 :
                                    intent = new Intent(view.getContext(), HomeActivity.class);
                                    break;
                                case 2 :
                                    intent = new Intent(view.getContext(), TeamListActivity.class);
                                    break;
                                case 3 :
                                    intent = new Intent(view.getContext(), Schedule.class);
                                    break;
                                case 4 :
                                    intent = new Intent(view.getContext(), PointTableGroupActivity.class);
                                    break;
                                case 5 :
                                    intent = new Intent(view.getContext(), LiveMatchActivity.class);
                                    break;
                                case 6 :
                                    intent = new Intent(view.getContext(), HomeActivity.class);
                                    break;
                                case 7 :
                                    intent = new Intent(view.getContext(), LiveTvActivity.class);
                                    break;
                                case 8 :
                                    Intent shareIntent = new Intent();
                                    shareIntent.setAction(Intent.ACTION_SEND);
                                    shareIntent.putExtra(Intent.EXTRA_TEXT, "Hey check out my app at: https://play.google.com/store/apps/details?id=" + view.getContext().getPackageName());
                                    shareIntent.setType("text/plain");
                                    view.getContext().startActivity(Intent.createChooser(shareIntent, view.getContext().getResources().getText(R.string.send_to)));
                                    break;
                                case 9 :
                                    intent = new Intent(view.getContext(), AboutActivity.class);
                                    break;
                                default:
                                    break;
                            }

//                            else if (drawerItem.getIdentifier() == 4) {
//
//                            } else if (drawerItem.getIdentifier() == 5) {
////                                Uri uri = Uri.parse("https://play.google.com/store/apps/developer?id=Datatrix+Soft");
//                                Uri uri = Uri.parse("https://play.google.com/store/apps/");
//                                Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
//                                try {
//                                    view.getContext().startActivity(myAppLinkToMarket);
//                                } catch (ActivityNotFoundException e) {
//                                    Toast.makeText(view.getContext(), " unable to find market app", Toast.LENGTH_LONG).show();
//                                }
//
//                            }

                            if (intent != null) {
                                if (position != 7 && position != 9){
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                }
                                view.getContext().startActivity(intent);
                            }
                        }
                        return false;
                    }
                })
                .build();
    }
}
