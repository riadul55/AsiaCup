package tech.iotait.asiacup;

import android.app.Application;
import android.content.Context;

import com.google.android.gms.ads.MobileAds;

public class BaseApp extends Application {
    private static BaseApp instance;
    private String push_url = null;

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
        MobileAds.initialize(this, getContext().getString(R.string.app_id));
    }

    public static BaseApp getInstance() {
        return instance;
    }

    public static Context getContext(){
        return instance;
    }

    public void AppSdkInit (Context context){
        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(context, context.getResources().getString(R.string.app_id));
    }

    public void fullScreenAd(Context context){
        MobileAds.initialize(context, context.getResources().getString(R.string.ad_id_full));
    }

    public void bannerAd(Context context){
        MobileAds.initialize(context, context.getResources().getString(R.string.ad_id_banner));
    }

    public synchronized String getPushUrl(){
        String url = push_url;
        push_url = null;
        return url;
    }

    public synchronized void setPushUrl(String url){
        this.push_url = url;
    }
}
