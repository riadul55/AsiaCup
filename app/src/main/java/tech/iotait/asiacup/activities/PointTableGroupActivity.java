package tech.iotait.asiacup.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tech.iotait.asiacup.models.model_siam.PointTableResponse;
import tech.iotait.asiacup.interfaces.PointTableService;
import tech.iotait.asiacup.R;
import tech.iotait.asiacup.utilities.DrawerUtil;

public class PointTableGroupActivity extends AppCompatActivity {

     TextView a_country1_name, coun1_match, coun1_winn, coun1_lost, coun1_draw, coun1_pt, coun1_nrr;
     TextView a_country2_name, coun2_match, coun2_winn, coun2_lost, coun2_draw, coun2_pt, coun2_nrr;
     TextView a_country3_name, coun3_match, coun3_winn, coun3_lost, coun3_draw, coun3_pt, coun3_nrr;
     TextView b_country1_name, b_coun1_match, b_coun1_winn, b_coun1_lost, b_coun1_draw, b_coun1_pt, b_coun1_nrr;
     TextView b_country2_name, b_coun2_match, b_coun2_winn, b_coun2_lost, b_coun2_draw, b_coun2_pt, b_coun2_nrr;
     TextView b_country3_name, b_coun3_match, b_coun3_winn, b_coun3_lost, b_coun3_draw, b_coun3_pt, b_coun3_nrr;

     ImageView next;
    Context context;
    public static final String  baseUrl="http://score.bdixapps.com/";
    private PointTableService service;
    public AlertDialog loadingDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_table_group);
        this.context=this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(R.string.title_history);
        new DrawerUtil().getDrawer(this, toolbar);

        loadingDialog = new SpotsDialog.Builder().setContext(this).setTheme(R.style.Custom).build();
        loadingDialog.setCancelable(false);
        loadingDialog.show();

        init();

        loadData();
    }


    public  void init(){
        next=findViewById(R.id.next);
        /* Group A and country 1 */
        a_country1_name = findViewById(R.id.groupA_coun1);
        coun1_match = findViewById(R.id.gA_coun1_match);
        coun1_winn = findViewById(R.id.gA_coun1_winn);
        coun1_lost = findViewById(R.id.gA_coun1_lost);
        coun1_draw = findViewById(R.id.gA_coun1_draw);
        coun1_pt = findViewById(R.id.gA_coun1_pt);
        coun1_nrr = findViewById(R.id.gA_coun1_nrr);

        /* Group A and country 2 */
        a_country2_name = findViewById(R.id.groupA_coun2);
        coun2_match = findViewById(R.id.gA_coun2_match);
        coun2_winn = findViewById(R.id.gA_coun2_winn);
        coun2_lost = findViewById(R.id.gA_coun2_lost);
        coun2_draw = findViewById(R.id.gA_coun2_draw);
        coun2_pt = findViewById(R.id.gA_coun2_pt);
        coun2_nrr = findViewById(R.id.gA_coun2_nrr);

        /* Group A and country 3 */
        a_country3_name = findViewById(R.id.groupA_coun3);
        coun3_match = findViewById(R.id.gA_coun3_match);
        coun3_winn = findViewById(R.id.gA_coun3_winn);
        coun3_lost = findViewById(R.id.gA_coun3_lost);
        coun3_draw = findViewById(R.id.gA_coun3_draw);
        coun3_pt = findViewById(R.id.gA_coun3_pt);
        coun3_nrr = findViewById(R.id.gA_coun3_nrr);

        /* Group B and country 1 */
        b_country1_name = findViewById(R.id.groupB_coun1);
        b_coun1_match = findViewById(R.id.gB_coun1_match);
        b_coun1_winn = findViewById(R.id.gB_coun1_winn);
        b_coun1_lost = findViewById(R.id.gB_coun1_lost);
        b_coun1_draw = findViewById(R.id.gB_coun1_draw);
        b_coun1_pt = findViewById(R.id.gB_coun1_pt);
        b_coun1_nrr = findViewById(R.id.gB_coun1_nrr);

        /* Group B and country 2 */
        b_country2_name = findViewById(R.id.groupB_coun2);
        b_coun2_match = findViewById(R.id.gB_coun2_match);
        b_coun2_winn = findViewById(R.id.gB_coun2_winn);
        b_coun2_lost = findViewById(R.id.gB_coun2_lost);
        b_coun2_draw = findViewById(R.id.gB_coun2_draw);
        b_coun2_pt = findViewById(R.id.gB_coun2_pt);
        b_coun2_nrr = findViewById(R.id.gB_coun2_nrr);

        /* Group B and country 3 */
        b_country3_name = findViewById(R.id.groupB_coun3);
        b_coun3_match = findViewById(R.id.gB_coun3_match);
        b_coun3_winn = findViewById(R.id.gB_coun3_winn);
        b_coun3_lost = findViewById(R.id.gB_coun3_lost);
        b_coun3_draw = findViewById(R.id.gB_coun3_draw);
        b_coun3_pt = findViewById(R.id.gB_coun3_pt);
        b_coun3_nrr = findViewById(R.id.gB_coun3_nrr);
    }

    public void showToast(String msg){
        Toast.makeText(context, msg,
                Toast.LENGTH_LONG).show();
    }

    public void loadActivity(){
        Intent refresh = new Intent(context, HomeActivity.class);
        refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(refresh);
        this.finish();
    }

    public void  loadData(){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory
                        .create()).build();
        service =retrofit.create(PointTableService.class);
        Call<PointTableResponse> call =service.getAllItems();
        call.enqueue(new Callback<PointTableResponse>() {
            @Override
            public void onResponse(Call<PointTableResponse> call, Response<PointTableResponse> response) {
                if(response.code()==200){
                    PointTableResponse items =response.body();


                    try{
                        a_country1_name.setText(items.getItems().get(0).getName());
                        coun1_match.setText(items.getItems().get(0).getUrl());
                        coun1_winn.setText(items.getItems().get(0).getAltUrl());
                        coun1_lost.setText(items.getItems().get(0).getImage());
                        coun1_draw.setText(items.getItems().get(0).getAltImage());
                        coun1_pt.setText(items.getItems().get(0).getApk());
                        coun1_nrr.setText(items.getItems().get(0).getYitid());

                        a_country2_name.setText(items.getItems().get(1).getName());
                        coun2_match.setText(items.getItems().get(1).getUrl());
                        coun2_winn.setText(items.getItems().get(1).getAltUrl());
                        coun2_lost.setText(items.getItems().get(1).getImage());
                        coun2_draw.setText(items.getItems().get(1).getAltImage());
                        coun2_pt.setText(items.getItems().get(1).getApk());
                        coun2_nrr.setText(items.getItems().get(1).getYitid());

                        a_country3_name.setText(items.getItems().get(2).getName());
                        coun3_match.setText(items.getItems().get(2).getUrl());
                        coun3_winn.setText(items.getItems().get(2).getAltUrl());
                        coun3_lost.setText(items.getItems().get(2).getImage());
                        coun3_draw.setText(items.getItems().get(2).getAltImage());
                        coun3_pt.setText(items.getItems().get(2).getApk());
                        coun3_nrr.setText(items.getItems().get(2).getYitid());

                        b_country1_name.setText(items.getItems().get(3).getName());
                        b_coun1_match.setText(items.getItems().get(3).getUrl());
                        b_coun1_winn.setText(items.getItems().get(3).getAltUrl());
                        b_coun1_lost.setText(items.getItems().get(3).getImage());
                        b_coun1_draw.setText(items.getItems().get(3).getAltImage());
                        b_coun1_pt.setText(items.getItems().get(3).getApk());
                        b_coun1_nrr.setText(items.getItems().get(3).getYitid());

                        b_country2_name.setText(items.getItems().get(4).getName());
                        b_coun2_match.setText(items.getItems().get(4).getUrl());
                        b_coun2_winn.setText(items.getItems().get(4).getAltUrl());
                        b_coun2_lost.setText(items.getItems().get(4).getImage());
                        b_coun2_draw.setText(items.getItems().get(4).getAltImage());
                        b_coun2_pt.setText(items.getItems().get(4).getApk());
                        b_coun2_nrr.setText(items.getItems().get(4).getYitid());

                        b_country3_name.setText(items.getItems().get(5).getName());
                        b_coun3_match.setText(items.getItems().get(5).getUrl());
                        b_coun3_winn.setText(items.getItems().get(5).getAltUrl());
                        b_coun3_lost.setText(items.getItems().get(5).getImage());
                        b_coun3_draw.setText(items.getItems().get(5).getAltImage());
                        b_coun3_pt.setText(items.getItems().get(5).getApk());
                        b_coun3_nrr.setText(items.getItems().get(5).getYitid());

                        if(items.getItems().get(6).getName().equals("NIL")){
                            next.setVisibility(View.GONE);
                        }else{
                            next.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //showToast("boo");

                                    Intent intent = new Intent(context, PointTableFinalActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                        }

                        if (loadingDialog.isShowing())
                            loadingDialog.dismiss();
                    }catch (Exception e){
                        showToast("Something went wrong.");
                        loadActivity();

                    }

                }
            }

            @Override
            public void onFailure(Call<PointTableResponse> call, Throwable t) {
                if (loadingDialog.isShowing())
                    loadingDialog.dismiss();
                showToast("Something went wrong. Please check your network.");
                loadActivity();

            }
        });


    }
}
