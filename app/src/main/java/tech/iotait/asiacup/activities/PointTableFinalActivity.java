package tech.iotait.asiacup.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tech.iotait.asiacup.interfaces.ClickFromDialog;
import tech.iotait.asiacup.interfaces.ConnectionListener;
import tech.iotait.asiacup.models.model_siam.PointTableResponse;
import tech.iotait.asiacup.interfaces.PointTableService;
import tech.iotait.asiacup.R;
import tech.iotait.asiacup.utilities.ConnectionDetector;
import tech.iotait.asiacup.utilities.DrawerUtil;
import tech.iotait.asiacup.utilities.WifiConnection;
import tech.iotait.asiacup.widgets.NoInternateAlert;

public class PointTableFinalActivity extends AppCompatActivity implements ConnectionListener, ClickFromDialog {


    private ConnectionDetector con;

     TextView sf_country1_name, coun1_match, coun1_winn, coun1_lost, coun1_draw, coun1_pt, coun1_nrr;
     TextView sf_country2_name, coun2_match, coun2_winn, coun2_lost, coun2_draw, coun2_pt, coun2_nrr;
     TextView sf_country3_name, coun3_match, coun3_winn, coun3_lost, coun3_draw, coun3_pt, coun3_nrr;
     TextView sf_country4_name, coun4_match, coun4_winn, coun4_lost, coun4_draw, coun4_pt, coun4_nrr;

    TextView f_team1, f_team2;
    ImageView back;Context context;
    public static final String  baseUrl="http://score.bdixapps.com/";
    private PointTableService service;
    public AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point_table_final);
        this.context=this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(R.string.title_history);
        new DrawerUtil().getDrawer(this, toolbar);

        con = new ConnectionDetector(this, this);
        con.isConnectingToInternet();

        if (!con.isConnected()){
            this.finish();
        }

        //loadingDialog = new SpotsDialog(context, R.style.Custom);
        loadingDialog = new SpotsDialog.Builder().setContext(this).setTheme(R.style.Custom).build();
        loadingDialog.setCancelable(false);
        loadingDialog.show();
        init();
        loadData();

    }

    public void init(){
        /* Super Four and country 1 */
        sf_country1_name = findViewById(R.id.SF_coun1);
        coun1_match = findViewById(R.id.SF_coun1_match);
        coun1_winn = findViewById(R.id.SF_coun1_winn);
        coun1_lost = findViewById(R.id.SF_coun1_lost);
        coun1_draw = findViewById(R.id.SF_coun1_draw);
        coun1_pt = findViewById(R.id.SF_coun1_pt);
        coun1_nrr = findViewById(R.id.SF_coun1_nrr);

        /* Super Four and country 2 */
        sf_country2_name = findViewById(R.id.SF_coun2);
        coun2_match = findViewById(R.id.SF_coun2_match);
        coun2_winn = findViewById(R.id.SF_coun2_winn);
        coun2_lost = findViewById(R.id.SF_coun2_lost);
        coun2_draw = findViewById(R.id.SF_coun2_draw);
        coun2_pt = findViewById(R.id.SF_coun2_pt);
        coun2_nrr = findViewById(R.id.SF_coun2_nrr);

        /* Super Four and country 3 */
        sf_country3_name = findViewById(R.id.SF_coun3);
        coun3_match = findViewById(R.id.SF_coun3_match);
        coun3_winn = findViewById(R.id.SF_coun3_winn);
        coun3_lost = findViewById(R.id.SF_coun3_lost);
        coun3_draw = findViewById(R.id.SF_coun3_draw);
        coun3_pt = findViewById(R.id.SF_coun3_pt);
        coun3_nrr = findViewById(R.id.SF_coun3_nrr);

        /* Super Four and country 4 */
        sf_country4_name = findViewById(R.id.SF_coun4);
        coun4_match = findViewById(R.id.SF_coun4_match);
        coun4_winn = findViewById(R.id.SF_coun4_winn);
        coun4_lost = findViewById(R.id.SF_coun4_lost);
        coun4_draw = findViewById(R.id.SF_coun4_draw);
        coun4_pt = findViewById(R.id.SF_coun4_pt);
        coun4_nrr = findViewById(R.id.SF_coun4_nrr);


        f_team1 = findViewById(R.id.final_team1);
        f_team2 = findViewById(R.id.final_team2);
        back=findViewById(R.id.back);

    }

    public void  loadData(){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory
                        .create()).build();
        service =retrofit.create(PointTableService.class);
        Call<PointTableResponse> call =service.getAllItems();
        call.enqueue(new Callback<PointTableResponse>() {
            @Override
            public void onResponse(Call<PointTableResponse> call, Response<PointTableResponse> response) {
                if(response.code()==200){
                    PointTableResponse items =response.body();


                    try{
                        sf_country1_name.setText(items.getItems().get(6).getName());
                        coun1_match.setText(items.getItems().get(6).getUrl());
                        coun1_winn.setText(items.getItems().get(6).getAltUrl());
                        coun1_lost.setText(items.getItems().get(6).getImage());
                        coun1_draw.setText(items.getItems().get(6).getAltImage());
                        coun1_pt.setText(items.getItems().get(6).getApk());
                        coun1_nrr.setText(items.getItems().get(6).getYitid());

                        sf_country2_name.setText(items.getItems().get(7).getName());
                        coun2_match.setText(items.getItems().get(7).getUrl());
                        coun2_winn.setText(items.getItems().get(7).getAltUrl());
                        coun2_lost.setText(items.getItems().get(7).getImage());
                        coun2_draw.setText(items.getItems().get(7).getAltImage());
                        coun2_pt.setText(items.getItems().get(7).getApk());
                        coun2_nrr.setText(items.getItems().get(7).getYitid());

                        sf_country3_name.setText(items.getItems().get(8).getName());
                        coun3_match.setText(items.getItems().get(8).getUrl());
                        coun3_winn.setText(items.getItems().get(8).getAltUrl());
                        coun3_lost.setText(items.getItems().get(8).getImage());
                        coun3_draw.setText(items.getItems().get(8).getAltImage());
                        coun3_pt.setText(items.getItems().get(8).getApk());
                        coun3_nrr.setText(items.getItems().get(8).getYitid());

                        sf_country4_name.setText(items.getItems().get(9).getName());
                        coun4_match.setText(items.getItems().get(9).getUrl());
                        coun4_winn.setText(items.getItems().get(9).getAltUrl());
                        coun4_lost.setText(items.getItems().get(9).getImage());
                        coun4_draw.setText(items.getItems().get(9).getAltImage());
                        coun4_pt.setText(items.getItems().get(9).getApk());
                        coun4_nrr.setText(items.getItems().get(9).getYitid());

                        f_team1.setText(items.getItems().get(10).getName());
                        f_team2.setText(items.getItems().get(11).getName());

                        if(loadingDialog.isShowing())
                            loadingDialog.dismiss();



                        back.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent refresh = new Intent(context, PointTableGroupActivity.class);
                                    refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(refresh);
                                    finish();
                                }
                        });
                    }catch (Exception e){
                        showToast("Something went wrong.");
                        loadActivity();

                    }

                }
            }

            @Override
            public void onFailure(Call<PointTableResponse> call, Throwable t) {
                if(loadingDialog.isShowing())
                   loadingDialog.dismiss();
                showToast("Something went wrong. Please check your network.");
                loadActivity();

            }
        });


    }

    public void showToast(String msg){
        Toast.makeText(context, msg,
                Toast.LENGTH_LONG).show();
    }

    public void loadActivity(){
        Intent refresh = new Intent(context, PointTableGroupActivity.class);
        refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(refresh);
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!con.isConnected()){
            showAlertDialog("Connection Error!!");
        }
    }

    @Override
    public void onClickContinue() {

    }

    @Override
    public void onClickFinish() {
        this.finish();
    }

    @Override
    public void onClickWifiCon() {
        new WifiConnection(this);
    }

    @Override
    public void onConnectionSuccess() {
    }

    @Override
    public void onConnectionFail(String errorMsg) {
        showAlertDialog(errorMsg);
    }

    private void showAlertDialog(String errorMsg) {
        FragmentManager fm = getSupportFragmentManager();
        NoInternateAlert alertDialog = new NoInternateAlert("Alert", errorMsg, this);
        alertDialog.show(fm, "fragment_alert");
    }

}
