package tech.iotait.asiacup.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import android.widget.Toast;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tech.iotait.asiacup.R;
import tech.iotait.asiacup.adapters.UpcomingMatchAdapter;
import tech.iotait.asiacup.interfaces.ClickFromDialog;
import tech.iotait.asiacup.interfaces.ConnectionListener;
import tech.iotait.asiacup.models.model_siam.UpcomingMatchesResponse;
import tech.iotait.asiacup.interfaces.UpcomingMatchesService;
import tech.iotait.asiacup.utilities.ConnectionDetector;
import tech.iotait.asiacup.utilities.DrawerUtil;
import tech.iotait.asiacup.utilities.WifiConnection;
import tech.iotait.asiacup.widgets.NoInternateAlert;

public class Schedule extends AppCompatActivity implements ConnectionListener, ClickFromDialog {
    Context context;
    private ConnectionDetector con;

    ListView simpleList;
    public static final String  baseUrl="http://score.bdixapps.com/";
    private UpcomingMatchesService service;

    public AlertDialog loadingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        this.context=this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(R.string.title_history);
        new DrawerUtil().getDrawer(this, toolbar);

        con = new ConnectionDetector(this, this);
        con.isConnectingToInternet();

        if (!con.isConnected()){
            this.finish();
        }

        loadingDialog = new SpotsDialog.Builder().setContext(this).setTheme(R.style.Custom).build();
        loadingDialog.setCancelable(false);
        loadingDialog.show();

        simpleList = findViewById(R.id.list);

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory
                        .create()).build();
        service =retrofit.create(UpcomingMatchesService.class);
        Call<UpcomingMatchesResponse> call =service.getAllItems();
        call.enqueue(new Callback<UpcomingMatchesResponse>() {
            @Override
            public void onResponse(Call<UpcomingMatchesResponse> call, Response<UpcomingMatchesResponse> response) {
                if(response.code()==200){
                    UpcomingMatchesResponse items =response.body();

                    UpcomingMatchAdapter upcomingMatchAdapter = new UpcomingMatchAdapter(Schedule.this, items);

                    simpleList.setAdapter(upcomingMatchAdapter);
                    try{
                        if (loadingDialog.isShowing())
                            loadingDialog.dismiss();
                    }catch (Exception e){
                        loadActivity();

                    }

                }
            }

            @Override
            public void onFailure(Call<UpcomingMatchesResponse> call, Throwable t) {
                if (loadingDialog.isShowing())
                    loadingDialog.dismiss();
                showToast("Something went wrong...");
                loadActivity();

            }
        });


    }

    public void showToast(String msg){
        Toast.makeText(context, msg,
                Toast.LENGTH_LONG).show();
    }

    public void loadActivity(){
        Intent refresh = new Intent(context, HomeActivity.class);
        refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(refresh);
        this.finish();
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (!con.isConnected()){
            showAlertDialog("Connection Error!!");
        }
    }

    @Override
    public void onClickContinue() {

    }

    @Override
    public void onClickFinish() {
        this.finish();
    }

    @Override
    public void onClickWifiCon() {
        new WifiConnection(this);
    }

    @Override
    public void onConnectionSuccess() {
    }

    @Override
    public void onConnectionFail(String errorMsg) {
        showAlertDialog(errorMsg);
    }

    private void showAlertDialog(String errorMsg) {
        FragmentManager fm = getSupportFragmentManager();
        NoInternateAlert alertDialog = new NoInternateAlert("Alert", errorMsg, this);
        alertDialog.show(fm, "fragment_alert");
    }

}
