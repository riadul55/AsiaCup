package tech.iotait.asiacup.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.view.MotionEvent;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import java.text.DecimalFormat;
import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tech.iotait.asiacup.interfaces.ClickFromDialog;
import tech.iotait.asiacup.interfaces.ConnectionListener;
import tech.iotait.asiacup.interfaces.LiveScoreCommentaryService;
import tech.iotait.asiacup.R;
import tech.iotait.asiacup.adapters.CommentaryAdapter;
import tech.iotait.asiacup.models.model_siam.CurrentMatchCommentaryLiveResponse;
import tech.iotait.asiacup.utilities.ConnectionDetector;
import tech.iotait.asiacup.utilities.DrawerUtil;
import tech.iotait.asiacup.utilities.WifiConnection;
import tech.iotait.asiacup.widgets.NoInternateAlert;

public class LiveScore extends AppCompatActivity implements ConnectionListener, ClickFromDialog {


    private ConnectionDetector con;

    public AlertDialog loadingDialog;
    private int mInterval = 10000; // 5 seconds by default, can be changed later
    private Handler mHandler;
    int flag=0;
    View customView;
    ScrollView scrollViewParent;
    double srStriker=0, srNonStriker=0;
    DecimalFormat df2 = new DecimalFormat(".##");
    int imgTeam1,imgTeam2;
    String previousBalls=" ",eco="0.00";
    String baseURL="http://synd.cricbuzz.com/iphone/3.0/match/";
    String commeataryDatapath="2018/2018_DULEEP/INDRED_INDBLUE_SEP04_SEP08/";
    ListView commentaryList;
    private LiveScoreCommentaryService service;
    Context context;
    boolean focus=true;

    String battingTeamName,ballingTeamName;
    ArrayList<String> teamList = new ArrayList<String>(4);

    ImageView team1logoIV;
    ImageView team2logoIV;
    TextView team1nameTV;
    TextView matchTypeTV;
    TextView dateOfMatechTV;
    TextView team2nameTV;
    TextView batingTeamNameTV;
    TextView battingTeamTV;
    TextView targetTV;
    TextView overTV;
    TextView crTV;
    TextView rrTV;
    TextView gameStatusTV;
    TextView batsmanNonStrikerTV;
    TextView batsmanStrikerTV;
    TextView runStrikerBatsmanTV;
    TextView runNonStrikerBatsmanTV;
    TextView ballStrikerBatsmanTV;
    TextView ballNonStrikerBatsmanTV;
    TextView strikerBatsmanFourTV;
    TextView nonStrikerBatsmanFourTV;
    TextView strikerBatsmanSixTV;
    TextView nonStrikerBatsmanSixTV;
    TextView strikerBatsmanSRTV;
    TextView nonStrikerBatsmanSRTV;
    TextView nameBowlerTV ;
    TextView overBowlerTV;
    TextView maidenOverBowlerTV;
    TextView runGivenBowlerTV;
    TextView wicketTakenBowlerTV;
    TextView ecoBowlerTV;
    TextView ballTV1,ballTV2,ballTV3,ballTV4,ballTV5,ballTV6,ballTV7,ballTV8,ballTV9,ballTV10,ballTV11,ballTV12,ballTV13;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_live_score);
        this.context=this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(R.string.title_history);
        new DrawerUtil().getDrawer(this, toolbar);
        con = new ConnectionDetector(this, this);
        con.isConnectingToInternet();

        if (!con.isConnected()){
            this.finish();
        }

        try {

            Bundle bundle=getIntent().getExtras();
            String s=bundle.getString("DATAPATH");
            commeataryDatapath=s;
        }catch (Exception e){
            loadActivity();
        }

        init();

        commentaryList = findViewById(R.id.list_commentary);
        scrollViewParent = (ScrollView)findViewById(R.id.scrollViewParent);

        customView = (View)findViewById(R.id.custom_commentary);

        commentaryList.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });

        loadingDialog = new SpotsDialog.Builder().setContext(this).setTheme(R.style.Custom).build();
        loadingDialog.show();
        loadingDialog.setCancelable(false);
        refresh();

        mHandler = new Handler();
        startRepeatingTask();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.refresh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_refresh :
                refresh();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
    }


    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                refresh();
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }

    public void init(){
        team1logoIV=findViewById(R.id.team1logoIVID);
        team2logoIV=findViewById(R.id.team2logoIVID);
        matchTypeTV=findViewById(R.id.matchTypeTVID);
        team1nameTV=findViewById(R.id.team1nameTVID);
        dateOfMatechTV=findViewById(R.id.dateOfMatechTVID);
        team2nameTV=findViewById(R.id.team2nameTVID);
        batingTeamNameTV=findViewById(R.id.batingTeamNameTVID);
        battingTeamTV=findViewById(R.id.battingTeamTVID);
        targetTV=findViewById(R.id.targetTVID);
        overTV=findViewById(R.id.overTVID);
        crTV=findViewById(R.id.crTVID);
        rrTV=findViewById(R.id.rrTVID);
        gameStatusTV=findViewById(R.id.gameStatusTVID);
        batsmanNonStrikerTV=findViewById(R.id.batsmanNonStrikerTVID);
        batsmanStrikerTV=findViewById(R.id.batsmanStrikerTVID);
        runStrikerBatsmanTV=findViewById(R.id.runStrikerBatsmanTVID);
        runNonStrikerBatsmanTV=findViewById(R.id.runNonStrikerBatsmanTVID);
        ballStrikerBatsmanTV=findViewById(R.id.ballStrikerBatsmanTVID);;
        ballNonStrikerBatsmanTV=findViewById(R.id.ballNonStrikerBatsmanTVID);
        strikerBatsmanFourTV=findViewById(R.id.strikerBatsmanFourTVID);
        nonStrikerBatsmanFourTV=findViewById(R.id.nonStrikerBatsmanFourTVID);
        strikerBatsmanSixTV=findViewById(R.id.strikerBatsmanSixTVID);
        nonStrikerBatsmanSixTV=findViewById(R.id.nonStrikerBatsmanSixTVID);
        strikerBatsmanSRTV=findViewById(R.id.strikerBatsmanSRTVID);
        nonStrikerBatsmanSRTV=findViewById(R.id.nonStrikerBatsmanSRTVID);
        nameBowlerTV =findViewById(R.id.nameBowlerTVID);
        overBowlerTV=findViewById(R.id.overBowlerTVID);
        maidenOverBowlerTV=findViewById(R.id.maidenOverBowlerTVID);
        runGivenBowlerTV=findViewById(R.id.runGivenBowlerTVID);
        wicketTakenBowlerTV=findViewById(R.id.wicketTakenBowlerTVID);
        ecoBowlerTV=findViewById(R.id.ecoBowlerTVID);

        ballTV1=findViewById(R.id.ball1);
        ballTV2=findViewById(R.id.ball2);
        ballTV3=findViewById(R.id.ball3);
        ballTV4=findViewById(R.id.ball4);
        ballTV5=findViewById(R.id.ball5);
        ballTV6=findViewById(R.id.ball6);
        ballTV7=findViewById(R.id.ball7);
        ballTV8=findViewById(R.id.ball8);
        ballTV9=findViewById(R.id.ball9);
        ballTV10=findViewById(R.id.ball10);
        ballTV11=findViewById(R.id.ball11);
        ballTV12=findViewById(R.id.ball12);
        ballTV13=findViewById(R.id.ball13);
    }

    public String getTeamName(String id){
        int count,index = 1;
        for (count=0; count<4;count++){
            if (teamList.get(count).equals(id)){
                index=count;
            }
        }
        return teamList.get(index-1);

    }

    public static boolean isResource(Context context, int resId){
        if (context != null){
            try {
                return context.getResources().getResourceName(resId) != null;
            } catch (Resources.NotFoundException ignore) {
            }
        }
        return false;
    }

    public void lastBallsSet(String str){
        String reverse = "";

        for(int i = str.length() - 1; i >= 0; i--)
        {
            reverse = reverse + str.charAt(i);
        }

        reverse = reverse.replaceFirst(" ", "");
        String[] splited = reverse.split(" ");

        try{
            ballTV1.setText(splited[0]);

        }catch (Exception e){
            ballTV1.setText("x");

        }
        try{
            ballTV2.setText(splited[1]);

        }catch (Exception e){
            ballTV2.setText("x");

        }
        try{
            ballTV3.setText(splited[2]);

        }catch (Exception e){
            ballTV3.setText("x");

        }
        try{
            ballTV4.setText(splited[3]);

        }catch (Exception e){
            ballTV4.setText("x");

        }
        try{
            ballTV5.setText(splited[4]);

        }catch (Exception e){
            ballTV5.setText("x");

        }
        try{
            ballTV6.setText(splited[5]);

        }catch (Exception e){
            ballTV6.setText("x");

        }
        try{
            ballTV7.setText(splited[6]);

        }catch (Exception e){
            ballTV7.setText("x");

        }
        try{
            ballTV8.setText(splited[7]);

        }catch (Exception e){
            ballTV8.setText("x");

        }
        try{
            ballTV9.setText(splited[8]);

        }catch (Exception e){
            ballTV9.setText("x");

        }
        try{
            ballTV10.setText(splited[9]);

        }catch (Exception e){
            ballTV10.setText("x");

        }
        try{
            ballTV11.setText(splited[10]);

        }catch (Exception e){
            ballTV11.setText("x");

        }
        try{
            ballTV12.setText(splited[11]);

        }catch (Exception e){
            ballTV12.setText("x");

        }
        try{
            ballTV13.setText(splited[12]);

        }catch (Exception e){
            ballTV13.setText("x");

        }



    }

    public String ecoBowler(String over, String run){
        double c, sum, eco=0;
        String[] convert = over.split("\\.");
        double a = Double.parseDouble(convert[0]);
        double b;
        try {
            b = Double.parseDouble(convert[1]);
        }catch (Exception e){
            b=0.0;
        }
        c = (a*6);
        sum = (c + b);

        try{
            eco=Double.parseDouble(run)*100/sum;
        }catch (Exception e){
            eco=0.0;

        }
        return  String.format( "%.2f", eco );
    }

    public void refresh(){
        flag++;
        Retrofit retrofit=new Retrofit.Builder()
            .baseUrl(baseURL+commeataryDatapath)
            .addConverterFactory(GsonConverterFactory
                    .create()).build();
        service =retrofit.create(LiveScoreCommentaryService.class);
        Call<CurrentMatchCommentaryLiveResponse> call =service.getAllItems();
        call.enqueue(new Callback<CurrentMatchCommentaryLiveResponse>() {
            @Override
            public void onResponse(Call<CurrentMatchCommentaryLiveResponse> call, Response<CurrentMatchCommentaryLiveResponse> response) {
                if(response.code()==200){
                    CurrentMatchCommentaryLiveResponse items =response.body();

                    try{
                        CommentaryAdapter commentaryAdapter = new CommentaryAdapter(context, items);
                        commentaryList.setAdapter(commentaryAdapter);

                        teamList.add(0,items.getTeam1().getName());
                        teamList.add(1,items.getTeam1().getId());
                        teamList.add(2,items.getTeam2().getName());
                        teamList.add(3,items.getTeam2().getId());

                        previousBalls=items.getMiniscore().getPrevOvers();

                        try {
                            srStriker=(Double.parseDouble(items.getMiniscore().getStriker().getRuns()) )*100/(Double.parseDouble(items.getMiniscore().getStriker().getBalls()));
                        }catch (Exception e){
                            srStriker=0.0;
                        }

                        try {
                            srNonStriker=(Double.parseDouble(items.getMiniscore().getNonStriker().getRuns()) )*100/(Double.parseDouble(items.getMiniscore().getNonStriker().getBalls()));
                        }catch (Exception e){
                            srNonStriker=0.0;
                        }

                        try {
                            eco=ecoBowler(items.getMiniscore().getBowler().getOvers()+".",items.getMiniscore().getBowler().getRuns());
                        }catch (Exception e){
                            eco="0.0";
                        }

                        try {
                            imgTeam1=context.getResources().getIdentifier(context.getPackageName()+":drawable/"+items.getTeam1().getFName().toLowerCase(), null, null);
                            if(isResource(context,imgTeam1))
                                team1logoIV.setImageResource(imgTeam1);
                            else
                                team1logoIV.setImageResource(R.drawable.otherteam);
                        }catch (Exception e){

                            team1logoIV.setImageResource(R.drawable.otherteam);
                        }

                        try {
                            imgTeam2=context.getResources().getIdentifier(context.getPackageName()+":drawable/"+items.getTeam2().getFName().toLowerCase(), null, null);
                            if(isResource(context,imgTeam2))
                                team2logoIV.setImageResource(imgTeam2);
                            else
                                team2logoIV.setImageResource(R.drawable.otherteam);
                        }catch (Exception e){

                            team2logoIV.setImageResource(R.drawable.otherteam);
                        }

                        lastBallsSet(previousBalls);
                        team1nameTV.setText(items.getTeam1().getSName());
                        matchTypeTV.setText(items.getHeader().getType());
                        dateOfMatechTV.setText(items.getHeader().getStartdt());
                        team2nameTV.setText(items.getTeam2().getSName());
                        batingTeamNameTV.setText(getTeamName(items.getMiniscore().getBatteamid()));
                        battingTeamTV.setText(items.getMiniscore().getBatteamscore());
                        if(items.getMiniscore().getBowlteamscore().equals(""))
                            targetTV.setText(items.getTeam1().getSName()+ " bat first");
                        else
                            targetTV.setText("Target: "+items.getMiniscore().getBowlteamscore());

                        overTV.setText("Overs: "+items.getMiniscore().getOvers());
                        crTV.setText(items.getMiniscore().getCrr()+"\n"+"C.R");
                        rrTV.setText(items.getMiniscore().getRrr()+"\n"+"R.R");
                        gameStatusTV.setText(items.getHeader().getStatus());
                        batsmanStrikerTV.setText("*"+items.getMiniscore().getStriker().getFullName());
                        runStrikerBatsmanTV.setText(items.getMiniscore().getStriker().getRuns());
                        ballStrikerBatsmanTV.setText(items.getMiniscore().getStriker().getBalls());
                        strikerBatsmanFourTV.setText(items.getBatsman().get(0).getFours());
                        strikerBatsmanSixTV.setText(items.getBatsman().get(0).getSixes());
                        strikerBatsmanSRTV.setText(String.format( "%.2f", srStriker ));
                        nameBowlerTV.setText(items.getMiniscore().getBowler().getFullName());
                        overBowlerTV.setText(items.getMiniscore().getBowler().getOvers());
                        maidenOverBowlerTV.setText(items.getMiniscore().getBowler().getMaidens());
                        runGivenBowlerTV.setText(items.getMiniscore().getBowler().getRuns());
                        wicketTakenBowlerTV.setText(items.getMiniscore().getBowler().getWicket());
                        ecoBowlerTV.setText(eco);

                        try{
                            batsmanNonStrikerTV.setText(items.getMiniscore().getNonStriker().getFullName());
                            runNonStrikerBatsmanTV.setText(items.getMiniscore().getNonStriker().getRuns());
                            ballNonStrikerBatsmanTV.setText(items.getMiniscore().getNonStriker().getBalls());
                            nonStrikerBatsmanSRTV.setText(String.format( "%.2f", srNonStriker ));
                            nonStrikerBatsmanSixTV.setText(items.getBatsman().get(1).getSixes());
                            nonStrikerBatsmanFourTV.setText(items.getBatsman().get(1).getFours());

                        }catch (Exception e){
                            batsmanNonStrikerTV.setText("---");
                            runNonStrikerBatsmanTV.setText("-");
                            ballNonStrikerBatsmanTV.setText("-");
                            nonStrikerBatsmanSRTV.setText("-");
                            nonStrikerBatsmanSixTV.setText("-");
                            nonStrikerBatsmanFourTV.setText("-");

                        }
                        if(focus)
                            scrollViewParent.fullScroll(ScrollView.FOCUS_UP); //.fullScroll(View.FOCUS_UP);

                        focus=false;

                    }catch (Exception e){
                        loadActivity();
                    }

                }
                else Toast.makeText(LiveScore.this, "Something went wrong...",
                        Toast.LENGTH_LONG).show();

                try{
                    if (loadingDialog.isShowing())
                        loadingDialog.dismiss();
                }catch (Exception e){
                    loadActivity();
                }
            }

            @Override
            public void onFailure(Call<CurrentMatchCommentaryLiveResponse> call, Throwable t) {
                showToast("Something went wrong. Check internet connection.");
                try{
                    if(flag==1){
                        loadActivity();
                    }
                if (loadingDialog.isShowing())
                    loadingDialog.dismiss();

                }catch (Exception e){
                    loadActivity();
                }

            }
        });

    }

    public void showToast(String msg){
        Toast.makeText(context, msg,
                Toast.LENGTH_LONG).show();
    }
    public void loadActivity(){
        Intent refresh = new Intent(context, LiveMatchActivity.class);
        refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(refresh);
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!con.isConnected()){
            showAlertDialog("Connection Error!!");
        }
    }

    @Override
    public void onClickContinue() {

    }

    @Override
    public void onClickFinish() {
        this.finish();
    }

    @Override
    public void onClickWifiCon() {
        new WifiConnection(this);
    }

    @Override
    public void onConnectionSuccess() {
    }

    @Override
    public void onConnectionFail(String errorMsg) {
        showAlertDialog(errorMsg);
    }

    private void showAlertDialog(String errorMsg) {
        FragmentManager fm = getSupportFragmentManager();
        NoInternateAlert alertDialog = new NoInternateAlert("Alert", errorMsg, this);
        alertDialog.show(fm, "fragment_alert");
    }

}
