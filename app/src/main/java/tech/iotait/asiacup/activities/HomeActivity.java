package tech.iotait.asiacup.activities;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.mikepenz.materialdrawer.Drawer;
import com.squareup.picasso.Picasso;

import dmax.dialog.SpotsDialog;
import tech.iotait.asiacup.DataRequests.HttpsHelper;
import tech.iotait.asiacup.DataRequests.ResponseCallback;
import tech.iotait.asiacup.R;
import tech.iotait.asiacup.models.count_down.Item;
import tech.iotait.asiacup.preferences.AppPreferences;
import tech.iotait.asiacup.utilities.WifiConnection;
import tech.iotait.asiacup.widgets.NoInternateAlert;
import tech.iotait.asiacup.interfaces.ClickFromDialog;
import tech.iotait.asiacup.interfaces.ConnectionListener;
import tech.iotait.asiacup.models.count_down.CountDown;
import tech.iotait.asiacup.services.MyNotificationPublisher;
import tech.iotait.asiacup.utilities.ConnectionDetector;
import tech.iotait.asiacup.utilities.DrawerUtil;

public class HomeActivity extends BaseActivity implements View.OnClickListener, ResponseCallback<CountDown>, ConnectionListener, ClickFromDialog {
    private Context context;
//    private AdView mAdMobAdView;

    private AppPreferences preferences;

    private ConnectionDetector con;

    private HttpsHelper httpsHelper;
    protected AlertDialog loadingDialog;

    private Toolbar toolbar;
    private Drawer drawer;

    private LinearLayout button1;
    private LinearLayout button2;
    private LinearLayout button3;
    private LinearLayout button4;
    private LinearLayout button5;
    private LinearLayout button6;

    //----- inits
    private TextView Title_text;
    private TextView timer_title;
    private TextView Date;
    //---- first country
    private ImageView first_country_flag;
    private TextView first_country_name;
    //---- second country
    private ImageView second_country_flag;
    private TextView second_country_name;
    //----- count down
    private TextView count_down_time;
    private TextView minutes;
    private TextView seconds;


    SimpleDateFormat gmtDateFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");

    long delayNotification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        this.context = this;

        //navigation
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(R.string.title_history);
        drawer = new DrawerUtil().getDrawer(this, toolbar);
        preferences = new AppPreferences(context);

        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        button5 = findViewById(R.id.button5);
        button6 = findViewById(R.id.button6);

        init();

        con = new ConnectionDetector(this, this);
        con.isConnectingToInternet();


        if (con.isConnected()){
            button1.setOnClickListener(this);
            button2.setOnClickListener(this);
            button3.setOnClickListener(this);
            button4.setOnClickListener(this);
            button5.setOnClickListener(this);
            button6.setOnClickListener(this);


            httpsHelper = new HttpsHelper(context);
            loadingDialog = new SpotsDialog.Builder().setContext(this).setTheme(R.style.Custom).build();
            loadingDialog.setCancelable(false);


            loadingDialog.show();
            httpsHelper.getUpComingMatches(this);

        } else {
            showAlertDialog("Connection Error!!");
        }
    }

    private void init() {
        //----- inits
        Title_text = findViewById(R.id.Title_text);
        timer_title = findViewById(R.id.timer_title);
        Date = findViewById(R.id.Date);
        //---- first country
        first_country_flag = findViewById(R.id.first_country_flag);
        first_country_name = findViewById(R.id.first_country_name);
        //---- second country
        second_country_flag = findViewById(R.id.second_country_flag);
        second_country_name = findViewById(R.id.second_country_name);
        //----- count down
        count_down_time = findViewById(R.id.count_down_time);

//        mAdMobAdView = findViewById(R.id.admob_adview);
    }

    public void onBackPressed() {
        if (drawer != null && drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button1 :
                startActivity(new Intent(context, TeamListActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.button2 :
                startActivity(new Intent(context, Schedule.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.button3 :
                startActivity(new Intent(context, PointTableGroupActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.button4 :
                startActivity(new Intent(context, LiveMatchActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.button5 :
//                startActivity(new Intent(context, LiveMatchActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.button6 :
                startActivity(new Intent(context, LiveTvActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
        }
    }

    @Override
    public void onSuccess(CountDown data) {
        showLog(data.getItems().get(0).getImage());
        Item items = data.getItems().get(0);

        preferences.setTimeLong(items.getApk());

        if (!data.getItems().isEmpty()){
            Title_text.setText(items.getName() + ", " + items.getAltUrl() + ", " + items.getAltImage() + " Series " + Calendar.getInstance().get(Calendar.YEAR));


            first_country_name.setText(items.getName());
            second_country_name.setText(items.getAltUrl());

            preferences.setUpcomingMatch(items.getName() + " VS " + items.getAltUrl());

            Picasso.Builder builder = new Picasso.Builder(context);
            builder.downloader(new OkHttp3Downloader(context));

            builder.build().load(items.getUrl())
                    .placeholder(R.drawable.ic_flag)
                    .error(R.drawable.ic_flag)
                    .into(first_country_flag);

            builder.build().load(items.getImage())
                    .placeholder(R.drawable.ic_flag)
                    .error(R.drawable.ic_flag)
                    .into(second_country_flag);

            //set game time and notificationTittle here
            String gameTime= preferences.getTimeLong();
            String notificationTittle= preferences.getUpcomingMatch();

            showLog(gameTime);

            gmtDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            delayNotification = delay(gameTime,gmtDateFormat.format(new Date()));//replace gameTime and notificationTittle as notification dialogue
            if(delayNotification>0){
                try {
                    scheduleNotification(context, delayNotification , 001,notificationTittle);
                }catch (Exception e){

                }
            }

            long unix_seconds = Long.parseLong(preferences.getTimeLong()) / 1000;
            //convert seconds to milliseconds
            Date date = new Date(unix_seconds*1000L);
            // format of the date
            SimpleDateFormat jdf = new SimpleDateFormat("dd MMM, yyyy");
            jdf.setTimeZone(TimeZone.getDefault());
            String java_date = jdf.format(date);
            System.out.println("\n"+java_date+"\n");

            Date.setText(java_date);


            new CountDownTimer(delayNotification, 1000) {
                public void onTick(long millisUntilFinished) {
                    showLog("seconds remaining: " + millisUntilFinished / 1000);
                    int seconds = (int) (millisUntilFinished / 1000) % 60 ;
                    int minutes = (int) ((millisUntilFinished / (1000*60)) % 60);
                    int hours   = (int) ((millisUntilFinished / (1000*60*60)) % 24);

                    count_down_time.setText(String.format("%d:%d:%d",hours,minutes,seconds));
                }
                public void onFinish() {
                    if (con.isConnected()){
                        showLog("finish.....");
                    }
                }
            }.start();
        } else {
            timer_title.setVisibility(View.GONE);
        }
        loadingDialog.dismiss();
    }

    @Override
    public void onError(Throwable th) {
        showLog(th.getMessage());
        loadingDialog.dismiss();
    }

    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateGUI(intent);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
//        registerReceiver(br, new IntentFilter(TimerBroadcastService.COUNTDOWN_BR));
//        showLog("Registered broacast receiver");

        if (!con.isConnected()){
            showAlertDialog("Connection Error!!");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
//        unregisterReceiver(br);
//        showLog("Unregistered broacast receiver");
    }

    @Override
    protected void onStop() {
//        try {
//            unregisterReceiver(br);
//        } catch (Exception e) {
//            // Receiver was probably already stopped in onPause()
//        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
//        stopService(new Intent(this, TimerBroadcastService.class));
//        showLog("Stopped service");
//        mGcmNetworkManager.cancelAllTasks(ConnectionService.class);
        super.onDestroy();
    }


    private void updateGUI(Intent intent) {
        if (intent.getExtras() != null) {
            long millisUntilFinished = intent.getLongExtra("countdown", 0);
            showLog("Countdown seconds remaining: " +  millisUntilFinished / 1000);
        }
    }

    @Override
    public void onClickContinue() {

    }

    @Override
    public void onClickFinish() {
        this.finish();
    }

    @Override
    public void onClickWifiCon() {
        new WifiConnection(this);
    }

    @Override
    public void onConnectionSuccess() {
        showLog("Connection success");
    }

    @Override
    public void onConnectionFail(String errorMsg) {
        showLog(errorMsg);
        showAlertDialog(errorMsg);
    }

    private void showAlertDialog(String errorMsg) {
        FragmentManager fm = getSupportFragmentManager();
        NoInternateAlert alertDialog = new NoInternateAlert("Alert", errorMsg, this);
        alertDialog.show(fm, "fragment_alert");
    }
    public void scheduleNotification(Context context, long delay, int notificationId, String tittle) {//delay is after how much time(in millis) from current time you want to schedule the notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(tittle)
                .setContentText("Match starting now")
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.mipmap.ic_launcher)).getBitmap())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        Intent intent = new Intent(context, SplashActivity.class);
        PendingIntent activity = PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(activity);

        Notification notification = builder.build();

        Intent notificationIntent = new Intent(context, MyNotificationPublisher.class);
        notificationIntent.putExtra("notification_id", notificationId);
        notificationIntent.putExtra("notification", notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
        Log.d("Notification", "Notify created");
    }

    public Long delay(String matchTime, String gmtTime){

        DateFormat format = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date gmtTimeStamp = null;
        long diff=0;

        try {
            gmtTimeStamp =(Date) format.parse(gmtTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        diff=Long.valueOf(matchTime)-(gmtTimeStamp.getTime()+12*3600000);
        showLog(gmtTimeStamp.getTime()+""+diff/1000);
        return diff;

    }
}
