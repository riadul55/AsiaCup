package tech.iotait.asiacup.activities;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import java.util.Timer;
import java.util.TimerTask;

import tech.iotait.asiacup.R;
import tech.iotait.asiacup.widgets.NoInternateAlert;
import tech.iotait.asiacup.interfaces.ClickFromDialog;
import tech.iotait.asiacup.interfaces.ConnectionListener;
import tech.iotait.asiacup.utilities.ConnectionDetector;

public class SplashActivity extends BaseActivity implements ConnectionListener, ClickFromDialog {

    private ConnectionDetector con;
    private ProgressBar splashProgressBar;

    private Handler handler;
    private static final int SPLASH_SCREEN_DELAY_TIME = 2500;
    private Timer timer;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        splashProgressBar = findViewById(R.id.splashProgressBar);

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                startActivity(new Intent(SplashActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
////                startActivity(new Intent(SplashActivity.this, TeamListActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
//                finish();
//            }
//        }, 2500);

        con = new ConnectionDetector(this, this);
        con.isConnectingToInternet();
    }

    @Override
    public void onConnectionSuccess() {
        splashHandler();
    }

    @Override
    public void onConnectionFail(String errorMsg) {
        Log.d(TAG(), errorMsg);
        showAlertDialog(errorMsg);
    }

    @Override
    public void onClickContinue() {
        splashHandler();
    }

    @Override
    public void onClickFinish() {
        SplashActivity.this.finish();
    }

    @Override
    public void onClickWifiCon() {
        WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(true);
        showToast("Connecting Internet....");

        new CountDownTimer(10000, 1000) {
            public void onTick(long millisUntilFinished) {
                showLog("seconds remaining: " + millisUntilFinished / 1000);
                showToast("Connecting Internet...." + millisUntilFinished / 1000);
            }
            public void onFinish() {
                if (con.isConnected()){
                    splashHandler();
                    showLog("finish.....");
                }
            }
        }.start();
    }

    private void showAlertDialog(String errorMsg) {
        FragmentManager fm = getSupportFragmentManager();
        NoInternateAlert alertDialog = new NoInternateAlert("Info", errorMsg, this);
        alertDialog.show(fm, "fragment_alert");
    }

    private void splashHandler() {
        final long period = 20;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (count < 100) {
                    splashProgressBar.setProgress(count);
                    count++;
                } else {
                    timer.cancel();
                    startActivity(new Intent(SplashActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    SplashActivity.this.finish();
                }
            }
        }, 0, period);
    }
}
