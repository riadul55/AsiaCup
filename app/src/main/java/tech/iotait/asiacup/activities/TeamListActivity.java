package tech.iotait.asiacup.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tech.iotait.asiacup.BaseApp;
import tech.iotait.asiacup.DataRequests.HttpsHelper;
import tech.iotait.asiacup.DataRequests.ResponseCallback;
import tech.iotait.asiacup.R;
import tech.iotait.asiacup.RetrofitConfig.DataApi;
import tech.iotait.asiacup.RetrofitConfig.RetrofitClient;
import tech.iotait.asiacup.adapters.CountryListAdapter;
import tech.iotait.asiacup.fragments.TeamListFragment;
import tech.iotait.asiacup.interfaces.ClickFromDialog;
import tech.iotait.asiacup.interfaces.ConnectionListener;
import tech.iotait.asiacup.models.countryList.Countries;
import tech.iotait.asiacup.models.countryList.Country;
import tech.iotait.asiacup.utilities.ConnectionDetector;
import tech.iotait.asiacup.utilities.DrawerUtil;
import tech.iotait.asiacup.utilities.WifiConnection;
import tech.iotait.asiacup.widgets.NoInternateAlert;

public class TeamListActivity extends BaseActivity implements View.OnClickListener, ResponseCallback<Countries>, ConnectionListener, ClickFromDialog {

    private Context context;


    //Ad implements
    private AdLoader adLoader;
    public static final int NUMBER_OF_ADS = 3;

    private List<Object> mRecyclerViewItems = new ArrayList<>();
    private List<UnifiedNativeAd> mNativeAds = new ArrayList<>();


    private ConnectionDetector con;

    private RecyclerView countryLists;

    private HttpsHelper httpsHelper;
    protected AlertDialog loadingDialog;

    private CountryListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_list);
        this.context = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolBar(toolbar);
//        getSupportActionBar().setTitle(R.string.title_history);
        new DrawerUtil().getDrawer(this, toolbar);

        con = new ConnectionDetector(this, this);
        con.isConnectingToInternet();

        if (!con.isConnected()){
            this.finish();
        }

        countryLists = findViewById(R.id.countryLists);

        countryLists.setHasFixedSize(true);
        countryLists.setLayoutManager(new LinearLayoutManager(context));

        httpsHelper = new HttpsHelper(context);
        loadingDialog = new SpotsDialog.Builder().setContext(this).setTheme(R.style.Custom).build();
        loadingDialog.setCancelable(false);

        loadingDialog.show();
        httpsHelper.getCountryList(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        //
    }

    @Override
    public void onSuccess(Countries data) {
        Log.d(TAG(), data.getCountries().get(0).getName());

        mRecyclerViewItems.addAll(data.getCountries());

        // Update the RecyclerView item's list with native ads.
        loadNativeAds();

        generateDataList(mRecyclerViewItems);

        loadingDialog.dismiss();

    }

    @Override
    public void onError(Throwable th) {
        loadingDialog.dismiss();
        showLog("Something went wrong...");
    }

    private void loadNativeAds() {
        AdLoader.Builder builder = new AdLoader.Builder(this, this.getResources().getString(R.string.ad_id_rewarded));
        adLoader = builder.forUnifiedNativeAd(
                new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                    @Override
                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                        // A native ad loaded successfully, check if the ad loader has finished loading
                        // and if so, insert the ads into the list.
                        mNativeAds.add(unifiedNativeAd);
                        if (!adLoader.isLoading()) {
                            insertAdsInMenuItems();
                        }
                    }
                }).withAdListener(
                new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        // A native ad failed to load, check if the ad loader has finished loading
                        // and if so, insert the ads into the list.
                        Log.e("MainActivity", "The previous native ad failed to load. Attempting to" + " load another.");
                        if (!adLoader.isLoading()) {
                            insertAdsInMenuItems();
                        }
                    }
                }).build();

        // Load the Native ads.
        adLoader.loadAds(new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("9739E8EAFF4E19B64944B72F568F8E47")
                .build(), NUMBER_OF_ADS);
    }


    private void insertAdsInMenuItems() {
        if (mNativeAds.size() <= 0) {
            return;
        }

        int offset = (mRecyclerViewItems.size() / mNativeAds.size()) + 1;
        int index = 0;
        for (UnifiedNativeAd ad : mNativeAds) {
            mRecyclerViewItems.add(index, ad);
            index = index + offset;
        }
    }

    private void generateDataList(List<Object> objectList) {
        listAdapter = new CountryListAdapter(context, objectList);
        countryLists.setAdapter(listAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!con.isConnected()){
            showAlertDialog("Connection Error!!");
        }
    }

    @Override
    public void onClickContinue() {

    }

    @Override
    public void onClickFinish() {
        this.finish();
    }

    @Override
    public void onClickWifiCon() {
        new WifiConnection(this);
    }

    @Override
    public void onConnectionSuccess() {
    }

    @Override
    public void onConnectionFail(String errorMsg) {
        showAlertDialog(errorMsg);
    }

    private void showAlertDialog(String errorMsg) {
        FragmentManager fm = getSupportFragmentManager();
        NoInternateAlert alertDialog = new NoInternateAlert("Alert", errorMsg, this);
        alertDialog.show(fm, "fragment_alert");
    }

}
