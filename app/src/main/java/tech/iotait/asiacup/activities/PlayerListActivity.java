package tech.iotait.asiacup.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;
import tech.iotait.asiacup.DataRequests.HttpsHelper;
import tech.iotait.asiacup.DataRequests.ResponseCallback;
import tech.iotait.asiacup.R;
import tech.iotait.asiacup.adapters.CountryListAdapter;
import tech.iotait.asiacup.adapters.PlayerListAdapter;
import tech.iotait.asiacup.fragments.PlayerListFragment;
import tech.iotait.asiacup.fragments.TeamListFragment;
import tech.iotait.asiacup.interfaces.ClickFromDialog;
import tech.iotait.asiacup.interfaces.ConnectionListener;
import tech.iotait.asiacup.models.countryList.Countries;
import tech.iotait.asiacup.models.countryList.Country;
import tech.iotait.asiacup.models.countryList.Player;
import tech.iotait.asiacup.utilities.ConnectionDetector;
import tech.iotait.asiacup.utilities.WifiConnection;
import tech.iotait.asiacup.widgets.NoInternateAlert;

public class PlayerListActivity extends BaseActivity implements ResponseCallback<Countries> , ConnectionListener, ClickFromDialog {

    private Context context;
    private ConnectionDetector con;
    private RecyclerView playerLists;

    private AdView adView;

    private HttpsHelper httpsHelper;
    protected AlertDialog loadingDialog;

    private PlayerListAdapter listAdapter;

    private String position;
    private String country;
    private String url;

    private Countries countries;
    private List<Player> players;

    private TextView country_name;
    private ImageView country_flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_list);
        this.context = this;

        con = new ConnectionDetector(this, this);
        con.isConnectingToInternet();

        if (!con.isConnected()){
            this.finish();
        }
        playerLists = findViewById(R.id.playerLists);

        playerLists.setHasFixedSize(true);
        playerLists.setLayoutManager(new GridLayoutManager(context, 2));

        httpsHelper = new HttpsHelper(context);
        loadingDialog = new SpotsDialog.Builder().setContext(this).setTheme(R.style.Custom).build();
        loadingDialog.setCancelable(false);
        httpsHelper = new HttpsHelper(context);

        loadingDialog.show();
        httpsHelper.getCountryList(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            position = extras.getString("position");
            country = extras.getString("country");
            url = extras.getString("flag");
//            players = (List<Player>) extras.getSerializable("players");

//            Log.d(TAG(), players.toString());
        }
        showLog(String.valueOf(position));

        country_name = findViewById(R.id.country_name);
        country_flag = findViewById(R.id.country_flag);

        country_name.setText(country);


        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(url)
                .placeholder(R.drawable.ic_flag)
                .error(R.drawable.ic_flag)
                .into(country_flag);

//        countries = new Countries();
//
//        players = countries.getCountries().get(Integer.parseInt(position)).getPlayers();

//        generateDataList(players);


        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.toolbar_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolBar(toolbar);
        collapsingToolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        //Adview banner
        adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("9739E8EAFF4E19B64944B72F568F8E47")
                .build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
            }

            @Override
            public void onAdOpened() {
            }

            @Override
            public void onAdLeftApplication() {
            }

            @Override
            public void onAdClosed() {
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void onSuccess(Countries data) {
        Log.d(TAG(), data.getCountries().get(Integer.parseInt(position) -1).getPlayers().toString());
        generateDataList(data);
        loadingDialog.dismiss();
    }

    @Override
    public void onError(Throwable th) {
        loadingDialog.dismiss();
        showToast("Something went wrong...");
    }

    private void generateDataList(Countries countries) {
        listAdapter = new PlayerListAdapter(context, countries.getCountries().get(Integer.parseInt(position)-1).getPlayers());
        playerLists.setAdapter(listAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_add_transaction, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case android.R.id.home :
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (!con.isConnected()){
            showAlertDialog("Connection Error!!");
        }
    }

    @Override
    public void onClickContinue() {

    }

    @Override
    public void onClickFinish() {
        this.finish();
    }

    @Override
    public void onClickWifiCon() {
        new WifiConnection(this);
    }

    @Override
    public void onConnectionSuccess() {
    }

    @Override
    public void onConnectionFail(String errorMsg) {
        showAlertDialog(errorMsg);
    }

    private void showAlertDialog(String errorMsg) {
        FragmentManager fm = getSupportFragmentManager();
        NoInternateAlert alertDialog = new NoInternateAlert("Alert", errorMsg, this);
        alertDialog.show(fm, "fragment_alert");
    }
}
