package tech.iotait.asiacup.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import java.util.Iterator;
import java.util.List;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import android.app.AlertDialog;
import android.widget.Toast;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tech.iotait.asiacup.R;
import tech.iotait.asiacup.activities.HomeActivity;
import tech.iotait.asiacup.adapters.CurrentMatchesAdapter;
import tech.iotait.asiacup.interfaces.ClickFromDialog;
import tech.iotait.asiacup.interfaces.ConnectionListener;
import tech.iotait.asiacup.interfaces.CurrentMatchesService;
import tech.iotait.asiacup.models.model_siam.CurrentMatchesResponse;
import tech.iotait.asiacup.utilities.ConnectionDetector;
import tech.iotait.asiacup.utilities.DrawerUtil;
import tech.iotait.asiacup.utilities.WifiConnection;
import tech.iotait.asiacup.widgets.NoInternateAlert;

public class LiveMatchActivity extends AppCompatActivity implements ConnectionListener, ClickFromDialog {

    Context context;
    ListView simpleList;


    private ConnectionDetector con;

    public static final String  baseUrl="http://ams.mapps.cricbuzz.com/";
    private CurrentMatchesService service;

    public AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_match);

        this.context=this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(R.string.title_history);
        new DrawerUtil().getDrawer(this, toolbar);


        con = new ConnectionDetector(this, this);
        con.isConnectingToInternet();

        if (!con.isConnected()){
            this.finish();
        }

        loadingDialog = new SpotsDialog.Builder().setContext(this).setTheme(R.style.Custom).build();
        loadingDialog.setCancelable(false);

        loadingDialog.show();

        simpleList = findViewById(R.id.list_live_matches);

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory
                        .create()).build();
        service =retrofit.create(CurrentMatchesService.class);
        Call<List<CurrentMatchesResponse>> call =service.getAllItems();
        call.enqueue(new Callback<List<CurrentMatchesResponse>>() {
            @Override
            public void onResponse(Call<List<CurrentMatchesResponse>> call, Response<List<CurrentMatchesResponse>> response) {
                if(response.code()==200){
                    List<CurrentMatchesResponse> items =response.body();


                    if(items.isEmpty()){
                        showToast("No match found!");
                        loadActivity();

                    }
                    else {
                        for (Iterator<CurrentMatchesResponse> iter = items.listIterator(); iter.hasNext(); ) {
                            CurrentMatchesResponse a = iter.next();


                            if (!a.getHeader().getMchState().equals("complete")&&!a.getHeader().getMchState().equals("inprogress")&&!a.getHeader().getMchState().equals("stump")) {
                                iter.remove();
                            }
                        }
                        if(items.isEmpty()){
                            showToast("No match found!");
                            loadActivity();
                        }else
                        {
                            CurrentMatchesAdapter currentMatchesAdapter = new CurrentMatchesAdapter(context, items);
                            simpleList.setAdapter(currentMatchesAdapter);
                        }


                        try{
                            if (loadingDialog.isShowing())
                                loadingDialog.dismiss();
                        }catch (Exception e){

                            loadActivity();

                        }

                    }





                }
            }

            @Override
            public void onFailure(Call<List<CurrentMatchesResponse>> call, Throwable t) {
                try{
                    showToast("Something went wrong. Check internet connection.");
                    if (loadingDialog.isShowing())
                        loadingDialog.dismiss();
                    loadActivity();
                }catch (Exception e){
                    loadActivity();

                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!con.isConnected()){
            showAlertDialog("Connection Error!!");
        }
    }

    @Override
    public void onClickContinue() {

    }

    @Override
    public void onClickFinish() {
        this.finish();
    }

    @Override
    public void onClickWifiCon() {
        new WifiConnection(this);
    }

    @Override
    public void onConnectionSuccess() {
    }

    @Override
    public void onConnectionFail(String errorMsg) {
        showAlertDialog(errorMsg);
    }

    private void showAlertDialog(String errorMsg) {
        FragmentManager fm = getSupportFragmentManager();
        NoInternateAlert alertDialog = new NoInternateAlert("Alert", errorMsg, this);
        alertDialog.show(fm, "fragment_alert");
    }

    public void showToast(String msg){
        Toast.makeText(context, msg,
                Toast.LENGTH_LONG).show();
    }

    public void loadActivity(){
        Intent refresh = new Intent(context, HomeActivity.class);
        refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(refresh);
        this.finish();
    }

}
