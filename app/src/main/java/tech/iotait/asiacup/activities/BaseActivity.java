package tech.iotait.asiacup.activities;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

public abstract class BaseActivity extends AppCompatActivity {

    public String TAG(){
        return this.getClass().getSimpleName();
    }

    public void showToast(String str){
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    public void showLog(String str){
        Log.d(TAG(), str);
    }

    public void showSnackBar(){
    }

    public void setToolBar(Toolbar toolBar){
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
