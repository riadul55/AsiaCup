package tech.iotait.asiacup.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {
    private Context context;
    private SharedPreferences preferences;

    //reference preferences
    private static final String PID_ID = "pid_id";
    private static final String COUNTRY = "country";
    private static final String COUNTDOWNTIME = "countDownTime";
    private static final String UPCOMING_MATCH = "Upcoming";

    public AppPreferences(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences("AsiaCup", Context.MODE_PRIVATE);
    }

    public void setPidId(int id) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(PID_ID, id);
        editor.apply();
        editor.commit();
    }

    public int getPidId() {
        return preferences.getInt(PID_ID, 0);
    }

    public void setCountry(String country) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(COUNTRY, country);
        editor.apply();
        editor.commit();
    }

    public String getCountry() {
        return preferences.getString(COUNTRY, "Country");
    }


    public void setTimeLong(String time) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(COUNTDOWNTIME, time);
        editor.apply();
        editor.commit();
    }

    public String getTimeLong(){
        return preferences.getString(COUNTDOWNTIME, "");
    }


    public void setUpcomingMatch(String matches) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(UPCOMING_MATCH, matches);
        editor.apply();
        editor.commit();
    }

    public String getUpcomingMatch(){
        return preferences.getString(UPCOMING_MATCH, "");
    }
}
