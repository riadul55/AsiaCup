package tech.iotait.asiacup.DataRequests;

import android.content.Context;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tech.iotait.asiacup.RetrofitConfig.ApiUtils;
import tech.iotait.asiacup.RetrofitConfig.DataApi;
import tech.iotait.asiacup.models.count_down.CountDown;
import tech.iotait.asiacup.models.countryList.Countries;
import tech.iotait.asiacup.models.player.Players;
import tech.iotait.asiacup.models.player.info.PlayerInfo;
import tech.iotait.asiacup.utilities.Constants;

public class HttpsHelper {
    private Context context;
    private DataApi bdixappsApi;
    private DataApi cricInfoApi;

    public HttpsHelper(Context context) {
        this.context = context;
        this.bdixappsApi = ApiUtils.getBdixappsApi();
        this.cricInfoApi = ApiUtils.getCricInfoApi();
    }

    public void getCountryList(final ResponseCallback<Countries> infoResponseCallback){
        String countries_url = "json/country.json";
        bdixappsApi.getCountryList(countries_url).enqueue(new Callback<Countries>() {
            @Override
            public void onResponse(Call<Countries> call, Response<Countries> response) {
                Log.e("Http", "response res : " + response.toString());
                if (response.isSuccessful()){
                    if (response.code() == 200){
                        infoResponseCallback.onSuccess(response.body());
                    }else {
                        infoResponseCallback.onError(new Exception(response.message()));
                    }
                } else {
                    infoResponseCallback.onError(new Exception(response.message()));
                }
            }

            @Override
            public void onFailure(Call<Countries> call, Throwable t) {
                infoResponseCallback.onError(t);
            }
        });
    }

    public void getUpComingMatches(final ResponseCallback<CountDown> responseCallback){
        String upcoming_match_url = "time/apk/items.json";
        bdixappsApi.getUpComingMatches(upcoming_match_url).enqueue(new Callback<CountDown>() {
            @Override
            public void onResponse(Call<CountDown> call, Response<CountDown> response) {
                if (response.isSuccessful()){
                    if (response.code() == 200){
                        responseCallback.onSuccess(response.body());
                    } else {
                        responseCallback.onError(new Exception(response.message()));
                    }
                } else {
                    responseCallback.onError(new Exception(response.message()));
                }
            }

            @Override
            public void onFailure(Call<CountDown> call, Throwable t) {
                responseCallback.onError(t);
            }
        });
    }

    public void findPlayer(String name, final ResponseCallback<Players> infoResponseCallback){
        String player_finder_url = "playerFinder?apikey="+ Constants.API_KEY +"&name=" + name;
        cricInfoApi.findPlayer(player_finder_url).enqueue(new Callback<Players>() {
            @Override
            public void onResponse(Call<Players> call, Response<Players> response) {
                Log.e("Http", "response res : " + response.toString());
                if (response.isSuccessful()){
                    if (response.code() == 200){
                        infoResponseCallback.onSuccess(response.body());
                    }else {
                        infoResponseCallback.onError(new Exception(response.message()));
                    }
                } else {
                    infoResponseCallback.onError(new Exception(response.message()));
                }
            }

            @Override
            public void onFailure(Call<Players> call, Throwable t) {
                infoResponseCallback.onError(t);
            }
        });
    }

    public void getPlayerInfo(String pid_id, final ResponseCallback<PlayerInfo> infoResponseCallback){
        String player_info_url = "playerStats?apikey="+ Constants.API_KEY +"&pid=" + pid_id;
        cricInfoApi.getPlayerInfo(player_info_url).enqueue(new Callback<PlayerInfo>() {
            @Override
            public void onResponse(Call<PlayerInfo> call, Response<PlayerInfo> response) {
                Log.e("Http", "response res : " + response.toString());
                if (response.isSuccessful()){
                    if (response.code() == 200){
                        infoResponseCallback.onSuccess(response.body());
                    }else {
                        infoResponseCallback.onError(new Exception(response.message()));
                    }
                } else {
                    infoResponseCallback.onError(new Exception(response.message()));
                }
            }

            @Override
            public void onFailure(Call<PlayerInfo> call, Throwable t) {
                infoResponseCallback.onError(t);
            }
        });
    }
}
