package tech.iotait.asiacup.DataRequests;

/**
 * Created by bijoy on 1/3/18.
 */

public interface ResponseCallback<T> {
    void onSuccess(T data);

    void onError(Throwable th);
}
