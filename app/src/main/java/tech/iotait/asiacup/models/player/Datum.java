
package tech.iotait.asiacup.models.player;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum implements Serializable
{

    @SerializedName("pid")
    @Expose
    private Integer pid;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("name")
    @Expose
    private String name;
    private final static long serialVersionUID = 2914619980939839013L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Datum() {
    }

    /**
     * 
     * @param name
     * @param pid
     * @param fullName
     */
    public Datum(Integer pid, String fullName, String name) {
        super();
        this.pid = pid;
        this.fullName = fullName;
        this.name = name;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
