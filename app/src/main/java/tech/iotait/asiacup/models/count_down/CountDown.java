
package tech.iotait.asiacup.models.count_down;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountDown implements Serializable
{

    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    private final static long serialVersionUID = -1997418329250236392L;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
