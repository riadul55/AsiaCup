
package tech.iotait.asiacup.models.countryList;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Countries implements Serializable
{

    @SerializedName("countries")
    @Expose
    private List<Country> countries = null;
    private final static long serialVersionUID = 8755370135676175013L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Countries() {
    }

    /**
     * 
     * @param countries
     */
    public Countries(List<Country> countries) {
        super();
        this.countries = countries;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

}
