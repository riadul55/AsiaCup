
package tech.iotait.asiacup.models.player.info;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Serializable
{

    @SerializedName("bowling")
    @Expose
    private Bowling bowling;
    @SerializedName("batting")
    @Expose
    private Batting batting;
    private final static long serialVersionUID = -3167178275288459806L;

    public Bowling getBowling() {
        return bowling;
    }

    public void setBowling(Bowling bowling) {
        this.bowling = bowling;
    }

    public Batting getBatting() {
        return batting;
    }

    public void setBatting(Batting batting) {
        this.batting = batting;
    }

}
