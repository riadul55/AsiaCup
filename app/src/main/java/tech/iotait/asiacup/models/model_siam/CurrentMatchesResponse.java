package tech.iotait.asiacup.models.model_siam;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class CurrentMatchesResponse {
    @SerializedName("matchId")
    @Expose
    private String matchId;
    @SerializedName("srsid")
    @Expose
    private String srsid;
    @SerializedName("srs")
    @Expose
    private String srs;
    @SerializedName("datapath")
    @Expose
    private String datapath;
    @SerializedName("header")
    @Expose
    private Header header;
    @SerializedName("miniscore")
    @Expose
    private Miniscore miniscore;
    @SerializedName("team1")
    @Expose
    private Team1 team1;
    @SerializedName("team2")
    @Expose
    private Team2 team2;
    @SerializedName("srs_category")
    @Expose
    private List<Integer> srsCategory = null;
    @SerializedName("valueAdd")
    @Expose
    private ValueAdd valueAdd;

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public String getSrsid() {
        return srsid;
    }

    public void setSrsid(String srsid) {
        this.srsid = srsid;
    }

    public String getSrs() {
        return srs;
    }

    public void setSrs(String srs) {
        this.srs = srs;
    }

    public String getDatapath() {
        return datapath;
    }

    public void setDatapath(String datapath) {
        this.datapath = datapath;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Miniscore getMiniscore() {
        return miniscore;
    }

    public void setMiniscore(Miniscore miniscore) {
        this.miniscore = miniscore;
    }

    public Team1 getTeam1() {
        return team1;
    }

    public void setTeam1(Team1 team1) {
        this.team1 = team1;
    }

    public Team2 getTeam2() {
        return team2;
    }

    public void setTeam2(Team2 team2) {
        this.team2 = team2;
    }

    public List<Integer> getSrsCategory() {
        return srsCategory;
    }

    public void setSrsCategory(List<Integer> srsCategory) {
        this.srsCategory = srsCategory;
    }

    public ValueAdd getValueAdd() {
        return valueAdd;
    }

    public void setValueAdd(ValueAdd valueAdd) {
        this.valueAdd = valueAdd;
    }

    public class Alerts {

        @SerializedName("enabled")
        @Expose
        private String enabled;
        @SerializedName("type")
        @Expose
        private String type;

        public String getEnabled() {
            return enabled;
        }

        public void setEnabled(String enabled) {
            this.enabled = enabled;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

    }

    public class Bowler {

        @SerializedName("fullName")
        @Expose
        private String fullName;
        @SerializedName("overs")
        @Expose
        private String overs;
        @SerializedName("maidens")
        @Expose
        private String maidens;
        @SerializedName("runs")
        @Expose
        private String runs;
        @SerializedName("wicket")
        @Expose
        private String wicket;

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getOvers() {
            return overs;
        }

        public void setOvers(String overs) {
            this.overs = overs;
        }

        public String getMaidens() {
            return maidens;
        }

        public void setMaidens(String maidens) {
            this.maidens = maidens;
        }

        public String getRuns() {
            return runs;
        }

        public void setRuns(String runs) {
            this.runs = runs;
        }

        public String getWicket() {
            return wicket;
        }

        public void setWicket(String wicket) {
            this.wicket = wicket;
        }

    }

    public class Header {

        @SerializedName("start_time")
        @Expose
        private String startTime;
        @SerializedName("end_time")
        @Expose
        private String endTime;
        @SerializedName("startdt")
        @Expose
        private String startdt;
        @SerializedName("stTme")
        @Expose
        private String stTme;
        @SerializedName("stTmeGMT")
        @Expose
        private String stTmeGMT;
        @SerializedName("enddt")
        @Expose
        private String enddt;
        @SerializedName("mnum")
        @Expose
        private String mnum;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("mchDesc")
        @Expose
        private String mchDesc;
        @SerializedName("mchState")
        @Expose
        private String mchState;
        @SerializedName("TW")
        @Expose
        private String tW;
        @SerializedName("decisn")
        @Expose
        private String decisn;
        @SerializedName("grnd")
        @Expose
        private String grnd;
        @SerializedName("vzone")
        @Expose
        private String vzone;
        @SerializedName("vcity")
        @Expose
        private String vcity;
        @SerializedName("vcountry")
        @Expose
        private String vcountry;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("addnStatus")
        @Expose
        private String addnStatus;
        @SerializedName("MOM")
        @Expose
        private String mOM;
        @SerializedName("NoOfIngs")
        @Expose
        private String noOfIngs;
        @SerializedName("timeDiff")
        @Expose
        private String timeDiff;

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getStartdt() {
            return startdt;
        }

        public void setStartdt(String startdt) {
            this.startdt = startdt;
        }

        public String getStTme() {
            return stTme;
        }

        public void setStTme(String stTme) {
            this.stTme = stTme;
        }

        public String getStTmeGMT() {
            return stTmeGMT;
        }

        public void setStTmeGMT(String stTmeGMT) {
            this.stTmeGMT = stTmeGMT;
        }

        public String getEnddt() {
            return enddt;
        }

        public void setEnddt(String enddt) {
            this.enddt = enddt;
        }

        public String getMnum() {
            return mnum;
        }

        public void setMnum(String mnum) {
            this.mnum = mnum;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMchDesc() {
            return mchDesc;
        }

        public void setMchDesc(String mchDesc) {
            this.mchDesc = mchDesc;
        }

        public String getMchState() {
            return mchState;
        }

        public void setMchState(String mchState) {
            this.mchState = mchState;
        }

        public String getTW() {
            return tW;
        }

        public void setTW(String tW) {
            this.tW = tW;
        }

        public String getDecisn() {
            return decisn;
        }

        public void setDecisn(String decisn) {
            this.decisn = decisn;
        }

        public String getGrnd() {
            return grnd;
        }

        public void setGrnd(String grnd) {
            this.grnd = grnd;
        }

        public String getVzone() {
            return vzone;
        }

        public void setVzone(String vzone) {
            this.vzone = vzone;
        }

        public String getVcity() {
            return vcity;
        }

        public void setVcity(String vcity) {
            this.vcity = vcity;
        }

        public String getVcountry() {
            return vcountry;
        }

        public void setVcountry(String vcountry) {
            this.vcountry = vcountry;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getAddnStatus() {
            return addnStatus;
        }

        public void setAddnStatus(String addnStatus) {
            this.addnStatus = addnStatus;
        }

        public String getMOM() {
            return mOM;
        }

        public void setMOM(String mOM) {
            this.mOM = mOM;
        }

        public String getNoOfIngs() {
            return noOfIngs;
        }

        public void setNoOfIngs(String noOfIngs) {
            this.noOfIngs = noOfIngs;
        }

        public String getTimeDiff() {
            return timeDiff;
        }

        public void setTimeDiff(String timeDiff) {
            this.timeDiff = timeDiff;
        }

    }

    public class Miniscore {

        @SerializedName("batteamid")
        @Expose
        private String batteamid;
        @SerializedName("batteamscore")
        @Expose
        private String batteamscore;
        @SerializedName("bowlteamid")
        @Expose
        private String bowlteamid;
        @SerializedName("bowlteamscore")
        @Expose
        private String bowlteamscore;
        @SerializedName("overs")
        @Expose
        private String overs;
        @SerializedName("bowlteamovers")
        @Expose
        private String bowlteamovers;
        @SerializedName("rrr")
        @Expose
        private String rrr;
        @SerializedName("crr")
        @Expose
        private String crr;
        @SerializedName("cprtshp")
        @Expose
        private String cprtshp;
        @SerializedName("prevOvers")
        @Expose
        private String prevOvers;
        @SerializedName("lWkt")
        @Expose
        private String lWkt;
        @SerializedName("oversleft")
        @Expose
        private String oversleft;
        @SerializedName("striker")
        @Expose
        private Striker striker;
        @SerializedName("nonStriker")
        @Expose
        private NonStriker nonStriker;
        @SerializedName("bowler")
        @Expose
        private Bowler bowler;
        @SerializedName("nsbowler")
        @Expose
        private Nsbowler nsbowler;

        public String getBatteamid() {
            return batteamid;
        }

        public void setBatteamid(String batteamid) {
            this.batteamid = batteamid;
        }

        public String getBatteamscore() {
            return batteamscore;
        }

        public void setBatteamscore(String batteamscore) {
            this.batteamscore = batteamscore;
        }

        public String getBowlteamid() {
            return bowlteamid;
        }

        public void setBowlteamid(String bowlteamid) {
            this.bowlteamid = bowlteamid;
        }

        public String getBowlteamscore() {
            return bowlteamscore;
        }

        public void setBowlteamscore(String bowlteamscore) {
            this.bowlteamscore = bowlteamscore;
        }

        public String getOvers() {
            return overs;
        }

        public void setOvers(String overs) {
            this.overs = overs;
        }

        public String getBowlteamovers() {
            return bowlteamovers;
        }

        public void setBowlteamovers(String bowlteamovers) {
            this.bowlteamovers = bowlteamovers;
        }

        public String getRrr() {
            return rrr;
        }

        public void setRrr(String rrr) {
            this.rrr = rrr;
        }

        public String getCrr() {
            return crr;
        }

        public void setCrr(String crr) {
            this.crr = crr;
        }

        public String getCprtshp() {
            return cprtshp;
        }

        public void setCprtshp(String cprtshp) {
            this.cprtshp = cprtshp;
        }

        public String getPrevOvers() {
            return prevOvers;
        }

        public void setPrevOvers(String prevOvers) {
            this.prevOvers = prevOvers;
        }

        public String getLWkt() {
            return lWkt;
        }

        public void setLWkt(String lWkt) {
            this.lWkt = lWkt;
        }

        public String getOversleft() {
            return oversleft;
        }

        public void setOversleft(String oversleft) {
            this.oversleft = oversleft;
        }

        public Striker getStriker() {
            return striker;
        }

        public void setStriker(Striker striker) {
            this.striker = striker;
        }

        public NonStriker getNonStriker() {
            return nonStriker;
        }

        public void setNonStriker(NonStriker nonStriker) {
            this.nonStriker = nonStriker;
        }

        public Bowler getBowler() {
            return bowler;
        }

        public void setBowler(Bowler bowler) {
            this.bowler = bowler;
        }

        public Nsbowler getNsbowler() {
            return nsbowler;
        }

        public void setNsbowler(Nsbowler nsbowler) {
            this.nsbowler = nsbowler;
        }

    }

    public class NonStriker {

        @SerializedName("fullName")
        @Expose
        private String fullName;
        @SerializedName("runs")
        @Expose
        private String runs;
        @SerializedName("balls")
        @Expose
        private String balls;
        @SerializedName("fours")
        @Expose
        private String fours;
        @SerializedName("sixes")
        @Expose
        private String sixes;

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getRuns() {
            return runs;
        }

        public void setRuns(String runs) {
            this.runs = runs;
        }

        public String getBalls() {
            return balls;
        }

        public void setBalls(String balls) {
            this.balls = balls;
        }

        public String getFours() {
            return fours;
        }

        public void setFours(String fours) {
            this.fours = fours;
        }

        public String getSixes() {
            return sixes;
        }

        public void setSixes(String sixes) {
            this.sixes = sixes;
        }

    }

    public class Nsbowler {

        @SerializedName("fullName")
        @Expose
        private String fullName;
        @SerializedName("overs")
        @Expose
        private String overs;
        @SerializedName("maidens")
        @Expose
        private String maidens;
        @SerializedName("runs")
        @Expose
        private String runs;
        @SerializedName("wicket")
        @Expose
        private String wicket;

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getOvers() {
            return overs;
        }

        public void setOvers(String overs) {
            this.overs = overs;
        }

        public String getMaidens() {
            return maidens;
        }

        public void setMaidens(String maidens) {
            this.maidens = maidens;
        }

        public String getRuns() {
            return runs;
        }

        public void setRuns(String runs) {
            this.runs = runs;
        }

        public String getWicket() {
            return wicket;
        }

        public void setWicket(String wicket) {
            this.wicket = wicket;
        }

    }

    public class Striker {

        @SerializedName("fullName")
        @Expose
        private String fullName;
        @SerializedName("runs")
        @Expose
        private String runs;
        @SerializedName("balls")
        @Expose
        private String balls;
        @SerializedName("fours")
        @Expose
        private String fours;
        @SerializedName("sixes")
        @Expose
        private String sixes;

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getRuns() {
            return runs;
        }

        public void setRuns(String runs) {
            this.runs = runs;
        }

        public String getBalls() {
            return balls;
        }

        public void setBalls(String balls) {
            this.balls = balls;
        }

        public String getFours() {
            return fours;
        }

        public void setFours(String fours) {
            this.fours = fours;
        }

        public String getSixes() {
            return sixes;
        }

        public void setSixes(String sixes) {
            this.sixes = sixes;
        }

    }

    public class Team1 {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("fName")
        @Expose
        private String fName;
        @SerializedName("sName")
        @Expose
        private String sName;
        @SerializedName("flag")
        @Expose
        private String flag;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFName() {
            return fName;
        }

        public void setFName(String fName) {
            this.fName = fName;
        }

        public String getSName() {
            return sName;
        }

        public void setSName(String sName) {
            this.sName = sName;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

    }

    public class Team2 {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("fName")
        @Expose
        private String fName;
        @SerializedName("sName")
        @Expose
        private String sName;
        @SerializedName("flag")
        @Expose
        private String flag;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFName() {
            return fName;
        }

        public void setFName(String fName) {
            this.fName = fName;
        }

        public String getSName() {
            return sName;
        }

        public void setSName(String sName) {
            this.sName = sName;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

    }

    public class ValueAdd {

        @SerializedName("audioURL")
        @Expose
        private List<Object> audioURL = null;
        @SerializedName("alerts")
        @Expose
        private Alerts alerts;

        public List<Object> getAudioURL() {
            return audioURL;
        }

        public void setAudioURL(List<Object> audioURL) {
            this.audioURL = audioURL;
        }

        public Alerts getAlerts() {
            return alerts;
        }

        public void setAlerts(Alerts alerts) {
            this.alerts = alerts;
        }

    }

}
