package tech.iotait.asiacup.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tech.iotait.asiacup.R;

public class PlayerListFragment extends BaseFragment {
    private int position;

    public static PlayerListFragment newInstance(int position) {
        PlayerListFragment fragment = new PlayerListFragment();
        
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        position = bundle.getInt("position", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_player_list, container, false);
        showToast(String.valueOf(position));
        return view;
    }

}
