package tech.iotait.asiacup.fragments.player_details;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import tech.iotait.asiacup.R;
import tech.iotait.asiacup.models.player.info.ODIs;
import tech.iotait.asiacup.models.player.info.ODIs_;
import tech.iotait.asiacup.models.player.info.PlayerInfo;

@SuppressLint("ValidFragment")
public class BowlingFragment extends Fragment {
    private PlayerInfo playerInfo;

    private TextView matches;
    private TextView innings;
    private TextView runs;
    private TextView balls;
    private TextView wickets;
    private TextView average;
    private TextView ecos;
    private TextView bbis;
    private TextView bbms;
    private TextView strikes;
    private TextView fourW;
    private TextView fiveW;

    public BowlingFragment(PlayerInfo playerInfo) {
        this.playerInfo = playerInfo;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bowling, container, false);

        matches = view.findViewById(R.id.matches);
        innings = view.findViewById(R.id.innings);
        runs = view.findViewById(R.id.runs);
        balls = view.findViewById(R.id.balls);
        wickets = view.findViewById(R.id.wickets);
        average = view.findViewById(R.id.average);
        ecos = view.findViewById(R.id.ecos);
        bbis = view.findViewById(R.id.bbis);
        bbms = view.findViewById(R.id.bbms);
        strikes = view.findViewById(R.id.strikes);
        fourW = view.findViewById(R.id.fourW);
        fiveW = view.findViewById(R.id.fiveW);

        ODIs odis = playerInfo.getData().getBowling().getODIs();


        try {
            matches.setText(odis.getMat());
            innings.setText(odis.getInns());
            runs.setText(odis.getRuns());
            balls.setText(odis.getBalls());
            wickets.setText(odis.getWkts());
            average.setText(odis.getAve());
            ecos.setText(odis.getEcon());
            bbis.setText(odis.getBBI());
            bbms.setText(odis.getBBM());
            strikes.setText(odis.getSR());
            fourW.setText(odis.get4w());
            fiveW.setText(odis.get5w());
        } catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }

}
