package tech.iotait.asiacup.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.content.res.AppCompatResources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import dmax.dialog.SpotsDialog;
import tech.iotait.asiacup.BaseApp;
import tech.iotait.asiacup.DataRequests.HttpsHelper;
import tech.iotait.asiacup.DataRequests.ResponseCallback;
import tech.iotait.asiacup.R;
import tech.iotait.asiacup.adapters.PlayerInfoAdapter;
import tech.iotait.asiacup.adapters.ScreenSlidePagerAdapter;
import tech.iotait.asiacup.customs.WrapContentViewPager;
import tech.iotait.asiacup.interfaces.FragmentDataPassing;
import tech.iotait.asiacup.models.player.Players;
import tech.iotait.asiacup.models.player.info.PlayerInfo;
import tech.iotait.asiacup.preferences.AppPreferences;

@SuppressLint("ValidFragment")
public class ProfileFragment extends BaseFragment implements ResponseCallback<PlayerInfo>, TabLayout.OnTabSelectedListener {
    private Context context;

    private int position;
    private String name;
    private String url;
    private String pidId;

    private HttpsHelper httpsHelper;
    protected AlertDialog loadingDialog;

    //--------------
    private CircularImageView player_img;
    private TextView player_name;
    private TextView player_country;
    private TabLayout tabLayout;
    private WrapContentViewPager player_details_pager;

//    ArrayList<Integer> pidIds = new ArrayList<Integer>();

//    FragmentDataPassing dataPassing;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        try {
//            if (context instanceof FragmentDataPassing){
//                dataPassing = (FragmentDataPassing) context;
//            } else {
//                throw new RuntimeException(context.toString() + " must implement FragmentDataPassing");
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
    }

    public ProfileFragment(int position, String name, String url, String pidId) {
        this.position = position;
        this.name = name;
        this.url = url;
        this.pidId = pidId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        this.context = getActivity();

        httpsHelper = new HttpsHelper(context);
        loadingDialog = new SpotsDialog.Builder().setContext(context).setTheme(R.style.Custom).build();
        loadingDialog.setCancelable(false);

        //----------------
        player_img = view.findViewById(R.id.player_img);
        player_name = view.findViewById(R.id.player_name);
        player_country = view.findViewById(R.id.player_country);

        tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Info"));
        tabLayout.addTab(tabLayout.newTab().setText("Batting"));
        tabLayout.addTab(tabLayout.newTab().setText("Bowling"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        player_details_pager = view.findViewById(R.id.player_details_pager);
        player_details_pager.setOffscreenPageLimit(20);
        player_details_pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
//        tabLayout.setupWithViewPager(player_details_pager);

        tabLayout.addOnTabSelectedListener(this);
        tabLayout.getTabAt(0).select();
        if (tabLayout.getTabAt(0).isSelected()){
            setTabBG(
                    R.drawable.tab_background_selected_left,
                    R.drawable.tab_background_unselected,
                    R.drawable.tab_background_unselected
            );
            player_details_pager.setCurrentItem(0);
        }

        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        if (!url.isEmpty()){
            builder.build().load(url)
                    .placeholder(R.drawable.ic_account_circle)
                    .error(R.drawable.ic_account_circle)
                    .into(player_img);
        }
        player_name.setText(name);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        player_country.setText(new AppPreferences(context).getCountry());

        loadingDialog.show();
        httpsHelper.getPlayerInfo(pidId, this);
    }

    @Override
    public void onSuccess(PlayerInfo data) {
        try {
            player_details_pager.setAdapter(new PlayerInfoAdapter(getChildFragmentManager(), tabLayout.getTabCount(), data));
        } catch (Exception e){
            e.printStackTrace();
        }
        loadingDialog.dismiss();
    }

    @Override
    public void onError(Throwable th) {
        loadingDialog.dismiss();
        showToast("something wrong!!");
//        dataPassing.onError(th);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int tabPosition = tabLayout.getSelectedTabPosition();
        if (tabPosition == 0){
            setTabBG(
                    R.drawable.tab_background_selected_left,
                    R.drawable.tab_background_unselected,
                    R.drawable.tab_background_unselected
            );
        } else if (tabPosition == 1){
            setTabBG(
                    R.drawable.tab_background_unselected,
                    R.drawable.tab_background_selected_mid,
                    R.drawable.tab_background_unselected
            );
        } else if (tabPosition == 2){
            setTabBG(
                    R.drawable.tab_background_unselected,
                    R.drawable.tab_background_unselected,
                    R.drawable.tab_background_selected_right
            );
        }
        player_details_pager.setCurrentItem(tab.getPosition());
    }


    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onDetach() {
        super.onDetach();
//        dataPassing = null;
    }

    private void setTabBG(int tab1, int tab2, int tab3) {
        ViewGroup tabStrip = (ViewGroup) tabLayout.getChildAt(0);
        View tabView1 = tabStrip.getChildAt(0);
        View tabView2 = tabStrip.getChildAt(1);
        View tabView3 = tabStrip.getChildAt(2);
        View tabView4 = tabStrip.getChildAt(3);

        if (tabView1 != null) {
            ViewCompat.setBackground(tabView1, AppCompatResources.getDrawable(tabView1.getContext(), tab1));
        }
        if (tabView2 != null) {
            ViewCompat.setBackground(tabView2, AppCompatResources.getDrawable(tabView2.getContext(), tab2));
        }
        if (tabView3 != null) {
            ViewCompat.setBackground(tabView3, AppCompatResources.getDrawable(tabView3.getContext(), tab3));
        }
    }
}
