package tech.iotait.asiacup.fragments.player_details;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import dmax.dialog.SpotsDialog;
import tech.iotait.asiacup.BaseApp;
import tech.iotait.asiacup.DataRequests.HttpsHelper;
import tech.iotait.asiacup.DataRequests.ResponseCallback;
import tech.iotait.asiacup.R;
import tech.iotait.asiacup.fragments.BaseFragment;
import tech.iotait.asiacup.fragments.ProfileFragment;
import tech.iotait.asiacup.interfaces.FragmentDataPassing;
import tech.iotait.asiacup.models.player.info.PlayerInfo;
import tech.iotait.asiacup.preferences.AppPreferences;

@SuppressLint("ValidFragment")
public class InfoFragment extends BaseFragment {

    private Context context;
    private HttpsHelper httpsHelper;
    protected AlertDialog loadingDialog;

    //---------inits
    private TextView player_born;
    private TextView birth_place;
    private TextView player_role;
    private TextView bat_style;

    //-- test
    private TextView batTest;
    private TextView ballTest;
    private TextView allTest;
    //-- odi
    private TextView batOdi;
    private TextView ballOdi;
    private TextView allOdi;
    //-- t20
    private TextView batTt;
    private TextView ballTt;
    private TextView allTt;

    private PlayerInfo playerInfo;

    public InfoFragment(PlayerInfo playerInfo) {
        this.playerInfo = playerInfo;
        Log.d("Info", playerInfo.toString());
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        try {
//            if (getParentFragment() instanceof FragmentDataPassing){
//                dataPassing = (FragmentDataPassing) getParentFragment();
//            } else {
//                throw new RuntimeException(context.toString() + " must implement ParentFragment");
//            }
//        } catch (Exception e){
//            e.printStackTrace();
//        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        this.context = getActivity();

//        httpsHelper = new HttpsHelper(context);
//        loadingDialog = new SpotsDialog.Builder().setContext(context).setTheme(R.style.Custom).build();

        //------------
        player_born = view.findViewById(R.id.player_born);
        birth_place = view.findViewById(R.id.birth_place);
        player_role = view.findViewById(R.id.player_role);
        bat_style = view.findViewById(R.id.bat_style);
        //----------
//        batTest = view.findViewById(R.id.batTest);
//        ballTest = view.findViewById(R.id.ballTest);
//        allTest = view.findViewById(R.id.allTest);
        //------------
//        batOdi = view.findViewById(R.id.batOdi);
//        ballOdi = view.findViewById(R.id.ballOdi);
//        allOdi = view.findViewById(R.id.allOdi);
        //------
//        batTt = view.findViewById(R.id.batTt);
//        ballTt = view.findViewById(R.id.ballTt);
//        allTt = view.findViewById(R.id.allTt);

//        int pidId = new AppPreferences(context).getPidId();
//        Log.d(TAG(), String.valueOf(pidId));
//        if (!(pidId == 0)){
//            loadingDialog.show();
//            httpsHelper.getPlayerInfo(pidId, this);
//        }


        try {
            String[] strings = playerInfo.getBorn().split(",");
            Log.d(TAG(), playerInfo.getBorn());
            Log.d(TAG(), String.valueOf(strings.length));
            if (strings.length >= 3){
                String born = strings[0] + ", " + strings[1];
                String place = strings[2];

                if (!strings[0].isEmpty() || !strings[1].isEmpty()){ player_born.setText(born);
                }
                if (!strings[2].isEmpty()){
                    birth_place.setText(place);
                }
            } else {
                String born = strings[0] + ", " + strings[1];
                if (!strings[0].isEmpty() || !strings[1].isEmpty()){
                    player_born.setText(born);
                }
            }

            player_role.setText(playerInfo.getPlayingRole());
            bat_style.setText(playerInfo.getPlayingRole());
        } catch (Exception e){
            e.printStackTrace();
        }

//        new AppPreferences(context).setCountry(data.getCountry());
        //-----
//        batTest.setText(data.getData().getBatting().getFirstClass());
//        loadingDialog.dismiss();

        return view;
    }

//    @Override
//    public void onSuccess(PlayerInfo data) {
//        String[] strings = data.getBorn().split(",");
//        Log.d(TAG(), data.getBorn());
//        Log.d(TAG(), String.valueOf(strings.length));
//        if (strings.length >= 3){
//            String born = strings[0] + ", " + strings[1];
//            String place = strings[2];
//
//            if (!strings[0].isEmpty() || !strings[1].isEmpty()){ player_born.setText(born);
//            }
//            if (!strings[2].isEmpty()){
//                birth_place.setText(place);
//            }
//        } else {
//            String born = strings[0] + ", " + strings[1];
//            if (!strings[0].isEmpty() || !strings[1].isEmpty()){
//                player_born.setText(born);
//            }
//        }
//
//        player_role.setText(data.getPlayingRole());
//        bat_style.setText(data.getPlayingRole());
//
//        new AppPreferences(context).setCountry(data.getCountry());
//        //-----
////        batTest.setText(data.getData().getBatting().getFirstClass());
//        loadingDialog.dismiss();
//    }
//
//    @Override
//    public void onError(Throwable th) {
//        loadingDialog.dismiss();
//    }

}
