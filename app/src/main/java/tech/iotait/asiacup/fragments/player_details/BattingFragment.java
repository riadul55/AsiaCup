package tech.iotait.asiacup.fragments.player_details;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import tech.iotait.asiacup.R;
import tech.iotait.asiacup.models.player.info.ODIs_;
import tech.iotait.asiacup.models.player.info.PlayerInfo;

@SuppressLint("ValidFragment")
public class BattingFragment extends Fragment {
    private PlayerInfo playerInfo;

    TextView matches;
    TextView innings;
    TextView runs;
    TextView highest;
    TextView averages;
    TextView strikes;
    TextView sixes;
    TextView fours;
    TextView ducks;
    TextView fifties;
    TextView oneHundreds;

    public BattingFragment(PlayerInfo playerInfo) {
        this.playerInfo = playerInfo;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_batting, container, false);

        matches = view.findViewById(R.id.matches);
        innings = view.findViewById(R.id.innings);
        runs = view.findViewById(R.id.runs);
        highest = view.findViewById(R.id.highest);
        averages = view.findViewById(R.id.averages);
        strikes = view.findViewById(R.id.strikes);
        sixes = view.findViewById(R.id.sixes);
        fours = view.findViewById(R.id.fours);
        ducks = view.findViewById(R.id.ducks);
        fifties = view.findViewById(R.id.fifties);
        oneHundreds = view.findViewById(R.id.oneHundreds);

        ODIs_ odis = playerInfo.getData().getBatting().getODIs();

        try {
            matches.setText(odis.getMat());
            innings.setText(odis.getInns());
            runs.setText(odis.getRuns());
            highest.setText(odis.getHS());
            averages.setText(odis.getAve());
            strikes.setText(odis.getSR());
            sixes.setText(odis.get6s());
            fours.setText(odis.get4s());
            ducks.setText(odis.getNO());
            fifties.setText(odis.get50());
            oneHundreds.setText(odis.get100());
        } catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }
}
