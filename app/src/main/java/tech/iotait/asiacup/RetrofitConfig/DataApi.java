package tech.iotait.asiacup.RetrofitConfig;

import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Url;
import tech.iotait.asiacup.models.count_down.CountDown;
import tech.iotait.asiacup.models.countryList.Countries;
import tech.iotait.asiacup.models.countryList.Country;
import tech.iotait.asiacup.models.player.Players;
import tech.iotait.asiacup.models.player.info.PlayerInfo;

public interface DataApi {
    @Headers("Content-Type: application/json")
    @GET()
    Call<Countries> getCountryList(@Url String s);

    @GET()
    Call<CountDown> getUpComingMatches(@Url String s);

    @GET()
    Call<Players> findPlayer(@Url String s);

    @GET()
    Call<PlayerInfo> getPlayerInfo(@Url String s);
}
