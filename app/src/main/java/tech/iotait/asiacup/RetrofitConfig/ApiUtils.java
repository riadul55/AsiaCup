package tech.iotait.asiacup.RetrofitConfig;

import tech.iotait.asiacup.utilities.Constants;

public class ApiUtils {
    public static DataApi getBdixappsApi() {
        return RetrofitClient.getBdixappsRetrofit(Constants.BDIX_APPS).create(DataApi.class);
    }
    public static DataApi getCricInfoApi() {
        return RetrofitClient.getCricInfoRetrofit(Constants.CRIC_API).create(DataApi.class);
    }
}
