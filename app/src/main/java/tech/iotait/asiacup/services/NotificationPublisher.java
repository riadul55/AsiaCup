package tech.iotait.asiacup.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import tech.iotait.asiacup.R;

public class NotificationPublisher extends BroadcastReceiver {

    public static String NOTIFICATION_ID = "notification_id";
    public static String NOTIFICATION = "notification";

    @Override
    public void onReceive(Context context, Intent intent) {
        int id = intent.getIntExtra(NOTIFICATION_ID, 0);

        // Notification
        Notification notification = new Notification.Builder(context)
                .setContentTitle("This is notification title")
                .setContentText("This is notification text")
                .setSmallIcon(R.mipmap.ic_launcher).build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // Notification Manager
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager .notify(id, notification);
    }
}
