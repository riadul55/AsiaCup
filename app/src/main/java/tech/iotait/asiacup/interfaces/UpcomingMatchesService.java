package tech.iotait.asiacup.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import tech.iotait.asiacup.models.model_siam.UpcomingMatchesResponse;

public interface UpcomingMatchesService {

    @GET("apk/items.json")
    Call<UpcomingMatchesResponse> getAllItems();
}
