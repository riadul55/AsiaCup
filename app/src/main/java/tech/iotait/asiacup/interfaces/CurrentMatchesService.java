package tech.iotait.asiacup.interfaces;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import tech.iotait.asiacup.models.model_siam.CurrentMatchesResponse;

public interface CurrentMatchesService {
    @GET("cbzandroid/2.0/currentmatches.json")
    Call<List<CurrentMatchesResponse>> getAllItems();
}
