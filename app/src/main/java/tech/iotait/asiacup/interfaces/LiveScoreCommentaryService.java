package tech.iotait.asiacup.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import tech.iotait.asiacup.models.model_siam.CurrentMatchCommentaryLiveResponse;

public interface LiveScoreCommentaryService {
    @GET("commentary.json")
    Call<CurrentMatchCommentaryLiveResponse> getAllItems();
}
