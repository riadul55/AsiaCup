package tech.iotait.asiacup.interfaces;

public interface ConnectionListener {
    void onConnectionSuccess();

    void onConnectionFail(String errorMsg);
}
