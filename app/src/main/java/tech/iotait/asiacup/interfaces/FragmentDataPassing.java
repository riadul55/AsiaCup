package tech.iotait.asiacup.interfaces;

import tech.iotait.asiacup.models.player.info.PlayerInfo;

public interface FragmentDataPassing {
    void onSuccess(PlayerInfo data);
    void onError(Throwable th);
}
