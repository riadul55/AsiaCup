package tech.iotait.asiacup.interfaces;

public interface ClickFromDialog {
    void onClickContinue();
    void onClickFinish();
    void onClickWifiCon();
}
