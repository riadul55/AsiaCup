package tech.iotait.asiacup.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import tech.iotait.asiacup.models.model_siam.PointTableResponse;

public interface PointTableService {
    @GET("point/apk/items.json")
    Call<PointTableResponse> getAllItems();
}
