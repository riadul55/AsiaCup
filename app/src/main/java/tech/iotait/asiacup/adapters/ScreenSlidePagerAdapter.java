package tech.iotait.asiacup.adapters;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import tech.iotait.asiacup.fragments.ProfileFragment;

public class ScreenSlidePagerAdapter extends FragmentPagerAdapter {
    private int NUM_PAGES;
    private ArrayList<String> names;
    private ArrayList<String> urls;
    private ArrayList<String> pidId;

    public ScreenSlidePagerAdapter(FragmentManager fm, int num_pages, ArrayList<String> names, ArrayList<String> urls, ArrayList<String> pidId) {
        super(fm);
        this.NUM_PAGES = num_pages;
        this.names = names;
        this.urls = urls;
        this.pidId = pidId;
    }

    @Override
    public Fragment getItem(int position) {
        ProfileFragment profileFragment = new ProfileFragment(position, names.get(position), urls.get(position), pidId.get(position));
        return profileFragment;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        // this method will be called for every fragment in the ViewPager
        if (object instanceof ProfileFragment) {
            return POSITION_UNCHANGED; // don't force a reload
        } else {
            // POSITION_NONE means something like: this fragment is no longer valid
            // triggering the ViewPager to re-build the instance of this fragment.
            return POSITION_NONE;
        }
    }
}
