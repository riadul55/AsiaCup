package tech.iotait.asiacup.adapters;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import tech.iotait.asiacup.models.model_siam.CurrentMatchesResponse;
import tech.iotait.asiacup.activities.LiveScore;
import tech.iotait.asiacup.R;

public class CurrentMatchesAdapter extends BaseAdapter  {
    Context context;
    LayoutInflater inflter;
    List<CurrentMatchesResponse> currentMatchesResponse;
    int imgTeam1,imgTeam2;
    String team1,team2;
    ArrayList<String> teamList = new ArrayList<String>(4);

    public CurrentMatchesAdapter(Context applicationContext, List<CurrentMatchesResponse> currentMatchesResponse) {
        this.context = applicationContext;
        this.currentMatchesResponse=currentMatchesResponse;
        inflter = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return currentMatchesResponse.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_live_row, null);

        CardView card = view.findViewById(R.id.card_view_live_matches);

        //if(currentMatchesResponse.get(i).getHeader().getMchState().equals("complete")) {


            teamList.add(0, currentMatchesResponse.get(i).getTeam1().getSName());
            teamList.add(1, currentMatchesResponse.get(i).getTeam1().getId());
            teamList.add(2, currentMatchesResponse.get(i).getTeam2().getSName());
            teamList.add(3, currentMatchesResponse.get(i).getTeam2().getId());

            TextView team1nameTV = (TextView) view.findViewById(R.id.team1nameID);
            TextView team2nameTV = (TextView) view.findViewById(R.id.team2nameID);
            TextView textView1 = (TextView) view.findViewById(R.id.textview1);
            TextView textView2 = (TextView) view.findViewById(R.id.textview2);
            TextView textView3 = (TextView) view.findViewById(R.id.textview3);
            TextView resultMatchTV = (TextView) view.findViewById(R.id.resultMatch);



            team1 = currentMatchesResponse.get(i).getTeam1().getFName().replaceAll("\\s", "").toLowerCase();
            team2 = currentMatchesResponse.get(i).getTeam2().getFName().replaceAll("\\s", "").toLowerCase();


            ImageView team1IV = (ImageView) view.findViewById(R.id.team1logoID);
            ImageView team2IV = (ImageView) view.findViewById(R.id.team2logoID);


        try {
            imgTeam1=context.getResources().getIdentifier(context.getPackageName()+":drawable/"+team1, null, null);
            if(isResource(context,imgTeam1))
                team1IV.setImageResource(imgTeam1);
            else
                team1IV.setImageResource(R.drawable.otherteam);
        }catch (Exception e){

            team1IV.setImageResource(R.drawable.otherteam);
        }

        try {
            imgTeam2=context.getResources().getIdentifier(context.getPackageName()+":drawable/"+team2, null, null);
            if(isResource(context,imgTeam2))
                team2IV.setImageResource(imgTeam2);
            else
                team2IV.setImageResource(R.drawable.otherteam);
        }catch (Exception e){

            team2IV.setImageResource(R.drawable.otherteam);
        }



            if (i == 0)
                card.setCardBackgroundColor(Color.parseColor("#05101b"));

            team1nameTV.setText(currentMatchesResponse.get(i).getTeam1().getSName());
            team2nameTV.setText(currentMatchesResponse.get(i).getTeam2().getSName());

            textView1.setText("---");
            textView2.setText("---");
            textView3.setText("---");

        if(currentMatchesResponse.get(i).getHeader().getMchState().equals("complete")){
            textView1.setText(getTeamName(currentMatchesResponse.get(i).getMiniscore().getBatteamid())+" "+
                    currentMatchesResponse.get(i).getMiniscore().getBatteamscore());
            textView2.setText(getTeamName(currentMatchesResponse.get(i).getMiniscore().getBowlteamid())+" "+
                    currentMatchesResponse.get(i).getMiniscore().getBowlteamscore());
            textView3.setText(currentMatchesResponse.get(i).getHeader().getType());
        }
        else {

            textView1.setText(getTeamName(currentMatchesResponse.get(i).getMiniscore().getBatteamid())+" "+
                currentMatchesResponse.get(i).getMiniscore().getBatteamscore());
            textView2.setText("Overs: "+currentMatchesResponse.get(i).getMiniscore().getOvers());
            if(currentMatchesResponse.get(i).getMiniscore().getBowlteamscore().equals("")){
                textView3.setText(getTeamName(currentMatchesResponse.get(i).getMiniscore().getBatteamid())+" bat first");

            }
            else{
                textView3.setText("Target: "+currentMatchesResponse.get(i).getMiniscore().getBowlteamscore());
                }
        }
            resultMatchTV.setText(currentMatchesResponse.get(i).getHeader().getStatus());
        if(currentMatchesResponse.get(i).getHeader().getMchState().equals("inprogress")) {
            resultMatchTV.setBackgroundColor(Color.parseColor("#41aa14"));
        }


        //}

        card.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(context, LiveScore.class);
                intent.putExtra("DATAPATH", currentMatchesResponse.get(i).getDatapath());
                context.startActivity(intent);
            }
        });


        return view;
    }

    public static boolean isResource(Context context, int resId){
        if (context != null){
            try {
                return context.getResources().getResourceName(resId) != null;
            } catch (Resources.NotFoundException ignore) {
            }
        }
        return false;
    }

    public String getTeamName(String id){
        int count,index = 1;
        for (count=0; count<4;count++){
            if (teamList.get(count).equals(id)){
                index=count;
            }
        }
        return teamList.get(index-1);

    }

}