package tech.iotait.asiacup.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import tech.iotait.asiacup.R;
import tech.iotait.asiacup.activities.PlayerListActivity;
import tech.iotait.asiacup.activities.ScreenSlidePagerActivity;
import tech.iotait.asiacup.models.countryList.Country;
import tech.iotait.asiacup.models.countryList.Player;

public class PlayerListAdapter extends RecyclerView.Adapter<PlayerListAdapter.ViewHolder>{
    private Context context;
    private List<Player> players;

    ArrayList<String> names = new ArrayList<String>();
    ArrayList<String> player_pic = new ArrayList<String>();
    ArrayList<String> pidIds = new ArrayList<String>();

    public PlayerListAdapter(Context context, List<Player> players) {
        this.context = context;
        this.players = players;
    }

    @Override
    public PlayerListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.player_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PlayerListAdapter.ViewHolder holder, int i) {
        if (!(players.get(i).getName().equals("") || players.get(i).getName() == null)){
            holder.player_name.setText(players.get(i).getName());
            holder.player_role.setText(players.get(i).getRole());
        }

        if (players.get(i).getUrl().equals("")){
            holder.player_photo.getResources().getDrawable(R.drawable.ic_account_circle);
        } else {
            Picasso.Builder builder = new Picasso.Builder(context);
            builder.downloader(new OkHttp3Downloader(context));
            builder.build().load(players.get(i).getUrl())
                    .placeholder(R.drawable.ic_account_circle)
                    .error(R.drawable.ic_account_circle)
                    .into(holder.player_photo);
        }


        names.add(players.get(i).getName());
        player_pic.add(players.get(i).getUrl());
        pidIds.add(players.get(i).getPid());
    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView player_photo;
        private TextView player_name;
        private TextView player_role;

        public ViewHolder(View itemView) {
            super(itemView);
            player_photo = itemView.findViewById(R.id.player_photo);
            player_name = itemView.findViewById(R.id.player_name);
            player_role = itemView.findViewById(R.id.player_role);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(context, ScreenSlidePagerActivity.class);
            intent.putExtra("position", getAdapterPosition());
            intent.putExtra("pages", players.size());
            intent.putStringArrayListExtra("names", names);
            intent.putStringArrayListExtra("urls", player_pic);
            intent.putStringArrayListExtra("pidIds", pidIds);
            context.startActivity(intent);

            Log.d("position", String.valueOf(getAdapterPosition() + 1));
            Log.d("pages", String.valueOf(players.size()));
            Log.d("names", names.toString());
            Log.d("urls", player_pic.toString());
        }
    }
}
