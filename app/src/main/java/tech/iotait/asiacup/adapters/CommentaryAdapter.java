package tech.iotait.asiacup.adapters;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import tech.iotait.asiacup.models.model_siam.CurrentMatchCommentaryLiveResponse;
import tech.iotait.asiacup.R;

public class CommentaryAdapter extends BaseAdapter {
    Context context;
    String ballEvent;
    LayoutInflater inflter;
    CurrentMatchCommentaryLiveResponse currentMatchCommentaryLiveResponse;
    public CommentaryAdapter(Context applicationContext,
                             CurrentMatchCommentaryLiveResponse
                                     currentMatchCommentaryLiveResponse) {
        this.context = applicationContext;
        this.currentMatchCommentaryLiveResponse=currentMatchCommentaryLiveResponse;
        inflter = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return currentMatchCommentaryLiveResponse.getCommlines().size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_commentary, null);
        TextView commentaryTV = (TextView)view.findViewById(R.id.commentarytvID);
        TextView overCommentaryTV = (TextView)view.findViewById(R.id.overCommentary);
        ballEvent=currentMatchCommentaryLiveResponse.getCommlines().get(i).getBallno();
        if(ballEvent.equals("")){
            ballEvent="Commentary";
        }
        commentaryTV.setText(currentMatchCommentaryLiveResponse.getCommlines().get(i).getCommtxt());
        overCommentaryTV.setText(ballEvent);



        return view;
    }

    public static boolean isResource(Context context, int resId){
        if (context != null){
            try {
                return context.getResources().getResourceName(resId) != null;
            } catch (Resources.NotFoundException ignore) {
            }
        }
        return false;
    }


}
