package tech.iotait.asiacup.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import tech.iotait.asiacup.R;
import tech.iotait.asiacup.activities.PlayerListActivity;
import tech.iotait.asiacup.activities.TeamListActivity;
import tech.iotait.asiacup.fragments.PlayerListFragment;
import tech.iotait.asiacup.models.countryList.Countries;
import tech.iotait.asiacup.models.countryList.Country;
import tech.iotait.asiacup.models.countryList.Player;
import tech.iotait.asiacup.widgets.populateNativeAdView;

public class CountryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<Object> recyclerItems;

    // A menu item view type.
    private static final int ITEM_VIEW_TYPE = 0;

    // The unified native ad view type.
    private static final int UNIFIED_NATIVE_AD_VIEW_TYPE = 1;

    public CountryListAdapter(Context context, List<Object> recyclerItems) {
        this.context = context;
        this.recyclerItems = recyclerItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        switch (viewType) {
            case UNIFIED_NATIVE_AD_VIEW_TYPE:
                View view1 = layoutInflater.inflate(R.layout.native_express_ad_container, viewGroup, false);
                return new UnifiedNativeAdViewHolder(view1);
            case ITEM_VIEW_TYPE:
                // Fall through.
            default:
                View view2 = layoutInflater.inflate(R.layout.country_list_item, viewGroup, false);
                return new ViewHolder(view2);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case UNIFIED_NATIVE_AD_VIEW_TYPE:
                UnifiedNativeAd nativeAd = (UnifiedNativeAd) recyclerItems.get(position);
                new populateNativeAdView(nativeAd, ((UnifiedNativeAdViewHolder) holder).getAdView());
                break;
            case ITEM_VIEW_TYPE:
                // fall through
            default:
                ViewHolder viewHolder = ((ViewHolder) holder);
                Country country = ((Country) recyclerItems.get(position));

                // Get the menu item image resource ID.
                viewHolder.country_name.setText(country.getName());

                Picasso.Builder builder = new Picasso.Builder(context);
                builder.downloader(new OkHttp3Downloader(context));
                builder.build().load(country.getUrl())
                        .placeholder(R.drawable.ic_flag)
                        .error(R.drawable.ic_flag)
                        .into(viewHolder.country_photo);

                viewHolder.position = country.getId();
        }
    }

    @Override
    public int getItemCount() {
        return recyclerItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView country_photo;
        private TextView country_name;
        private String position;

        public ViewHolder(View itemView) {
            super(itemView);
            country_photo = itemView.findViewById(R.id.country_photo);
            country_name = itemView.findViewById(R.id.country_name);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Country country = (Country) recyclerItems.get(this.getAdapterPosition());

            Intent intent = new Intent(context, PlayerListActivity.class);
            intent.putExtra("position", country.getId());
            intent.putExtra("country", country.getName());
            intent.putExtra("flag", country.getUrl());
//            intent.putExtra("players", (Serializable) countries.get(getAdapterPosition()).getPlayers());
            context.startActivity(intent);
        }
    }

    @Override
    public int getItemViewType(int position) {
        Object recyclerViewItem = recyclerItems.get(position);

        if (recyclerViewItem instanceof UnifiedNativeAd) {
            return UNIFIED_NATIVE_AD_VIEW_TYPE;
        }

        return ITEM_VIEW_TYPE;
    }
}
