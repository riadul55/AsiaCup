package tech.iotait.asiacup.adapters;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import tech.iotait.asiacup.R;
import tech.iotait.asiacup.models.model_siam.UpcomingMatchesResponse;

public class UpcomingMatchAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    UpcomingMatchesResponse upcomingMatchesResponse;
    int imgTeam1,imgTeam2;
    String team1,team2;

    public UpcomingMatchAdapter(Context applicationContext, UpcomingMatchesResponse upcomingMatchesResponse) {
        this.context = applicationContext;
        this.upcomingMatchesResponse=upcomingMatchesResponse;
        inflter = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return upcomingMatchesResponse.getItems().size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_upcoming, null);

        CardView card = view.findViewById(R.id.card_view_top);
        TextView team1nameTV = (TextView)view.findViewById(R.id.team1name);
        TextView team2nameTV = (TextView)view.findViewById(R.id.team2name);
        TextView matchDateTV = (TextView)view.findViewById(R.id.dateOfMatech);
        TextView matchTimeTV = (TextView)view.findViewById(R.id.timeOfMatch);
        TextView matchTypeTV = (TextView)view.findViewById(R.id.matchType);


        ImageView team1IV = (ImageView) view.findViewById(R.id.team1logo);
        ImageView team2IV = (ImageView) view.findViewById(R.id.team2logo);

        if(i==0)
            card.setCardBackgroundColor(Color.parseColor("#05101b"));

        team1nameTV.setText(upcomingMatchesResponse.getItems().get(i).getName());
        team2nameTV.setText(upcomingMatchesResponse.getItems().get(i).getAltUrl());
        matchDateTV.setText(upcomingMatchesResponse.getItems().get(i).getApk());
        matchTimeTV.setText(upcomingMatchesResponse.getItems().get(i).getYitid());
        matchTypeTV.setText(upcomingMatchesResponse.getItems().get(i).getAltImage());

        team1=upcomingMatchesResponse.getItems().get(i).getUrl().toLowerCase();
        team2=upcomingMatchesResponse.getItems().get(i).getImage().toLowerCase();


        imgTeam1=context.getResources().getIdentifier(context.getPackageName()+":drawable/"+team1, null, null);
        imgTeam2=context.getResources().getIdentifier(context.getPackageName()+":drawable/"+team2, null, null);

        try {
            if(isResource(context,imgTeam1))
                team1IV.setImageResource(imgTeam1);
            else
                team1IV.setImageResource(R.drawable.otherteam);

        }catch (Exception e){

            team1IV.setImageResource(R.drawable.otherteam);
        }

        try {

            if(isResource(context,imgTeam2))
                team2IV.setImageResource(imgTeam2);
            else
                team2IV.setImageResource(R.drawable.otherteam);
        }catch (Exception e){

            team2IV.setImageResource(R.drawable.otherteam);
        }



        return view;
    }

    public static boolean isResource(Context context, int resId){
        if (context != null){
            try {
                return context.getResources().getResourceName(resId) != null;
            } catch (Resources.NotFoundException ignore) {
            }
        }
        return false;
    }
}
