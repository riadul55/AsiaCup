package tech.iotait.asiacup.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import tech.iotait.asiacup.fragments.player_details.BattingFragment;
import tech.iotait.asiacup.fragments.player_details.BowlingFragment;
import tech.iotait.asiacup.fragments.player_details.InfoFragment;
import tech.iotait.asiacup.models.player.info.PlayerInfo;

public class PlayerInfoAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    private PlayerInfo playerInfo;

    public PlayerInfoAdapter(FragmentManager fm, int NumOfTabs, PlayerInfo playerInfo) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.playerInfo = playerInfo;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                InfoFragment tab1 = new InfoFragment(playerInfo);
                return tab1;
            case 1:
                BattingFragment tab2 = new BattingFragment(playerInfo);
                return tab2;
            case 2:
                BowlingFragment tab3 = new BowlingFragment(playerInfo);
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
