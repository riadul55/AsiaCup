package tech.iotait.asiacup.widgets;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import tech.iotait.asiacup.interfaces.ClickFromDialog;

@SuppressLint("ValidFragment")
public class NoInternateAlert extends DialogFragment {

    private String title;
    private String msg;
    private ClickFromDialog clickFromDialog;

    public NoInternateAlert(String title, String msg, ClickFromDialog clickFromDialog){
        this.title = title;
        this.msg = msg;
        this.clickFromDialog = clickFromDialog;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        String title = getArguments().getString("title");
//        String msg = getArguments().getString("msg");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(msg + "\n\nPlease check your internet connectivity and try again.");
        alertDialogBuilder.setPositiveButton("Wifi",  new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // on success
                clickFromDialog.onClickWifiCon();
                dialog.dismiss();
            }
        });

        alertDialogBuilder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                clickFromDialog.onClickFinish();
            }
        });

        alertDialogBuilder.setNeutralButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                clickFromDialog.onClickContinue();
            }
        });

        return alertDialogBuilder.create();
    }
}
